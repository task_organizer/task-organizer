var gulp = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass');


gulp.task('default', ['sass', 'fonts', 'images']);

// Handle Errors
function handleError(err) {
    gutil.log(err);
    this.emit('end');
}

// Icons & Fonts...
gulp.task('fonts', function () {
    gulp.src('node_modules/font-awesome/fonts/**.*')
        .pipe(gulp.dest('public/fonts'));
    gulp.src('node_modules/bootstrap/fonts/**.*')
        .pipe(gulp.dest('public/fonts'));
});

gulp.task('images', function () {
    gulp.src('frontend/images/**/*.*')
        .pipe(gulp.dest('public/images'));
});

// Sass Builder
gulp.task('sass', function () {
    gulp.src('frontend/styles/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        })).on('error', handleError)
        .pipe(concat('custom.css')).on('error', handleError)
        .pipe(gulp.dest('public/css')).on('error', handleError);

    gulp.src('public/sass/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        })).on('error', handleError)
        .pipe(gulp.dest('public/css'));
});

if (process.env.NODE_ENV !== 'production') {
    // Watcher
    gulp.task('watch', function () {
        gulp.watch('frontend/styles/**/*.scss', ['sass']);
        gulp.watch('public/sass/**/*.scss', ['sass']);
    });
}