'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');


module.exports = {
    context: __dirname + '/frontend',

    entry: {
        app: './app'
    },

    output: {
        path: __dirname + '/public/js/dist',
        filename: '[name].js',
        library: '[name]'
    },

    devtool: NODE_ENV === 'development' ? 'cheap-module-eval-source-map' : null,

    watch: NODE_ENV === 'development',

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new webpack.ProvidePlugin({
            _: 'lodash',
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new webpack.ContextReplacementPlugin(/node_modules\/moment\/locale/, /en-gb|de/),
        new webpack.ContextReplacementPlugin(/node_modules\/datejs\/src\/i18n/, /en-gb|de-DE/)
    ],

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js']
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js']
    },

    module: {

        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                include: __dirname + '/frontend',
                query: {
                    presets: ['env', 'es2015'],
                    plugins: [
                        'transform-regenerator',
                        'transform-object-rest-spread'
                    ]
                }
            }
        ],

        noParse: /\/node_modules\/(angular\/angular|jquery\/dist\/jquery|lodash)/

    }
};

if (NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                // don't show unreachable variables etc
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}