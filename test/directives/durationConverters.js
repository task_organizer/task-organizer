const {formatDuration, createDurationFromText} = require('../../frontend/common/directives/ui-elements/duration-input/durationConverters');
const moment = require('moment');
const {assert} = require('chai');

describe('creating correct moment duration from text string', () => {

    describe('minutes conversion', () => {

        it('converts 5m correctly', () => {

            const correctDuration = moment.duration({
                minutes: 5
            });

            const duration = createDurationFromText('5m');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts 10m correctly', () => {

            const correctDuration = moment.duration({
                minutes: 10
            });

            const duration = createDurationFromText('10m');

            assert.deepEqual(duration, correctDuration);
        });

        it('converts 30m correctly', () => {

            const correctDuration = moment.duration({hours: 0.5});

            const duration = createDurationFromText('30m');
            assert.deepEqual(duration, correctDuration);

        });

    });

    describe('hours conversion', () => {

        it('converts 1h correctly', () => {

            const correctDuration = moment.duration({
                hours: 1
            });

            const duration = createDurationFromText('1h');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts 3h correctly', () => {

            const correctDuration = moment.duration({
                hours: 3
            });

            const duration = createDurationFromText('3h');

            assert.deepEqual(duration, correctDuration);

        });

    });

    describe('hours and minutes conversion', () => {

        it('converts 1h5m correctly', () => {

            const correctDuration = moment.duration({
                hours: 1,
                minutes: 5
            });

            const duration = createDurationFromText('1h5m');

            assert.deepEqual(duration, correctDuration);

        });

    });

    describe('case/whitespace insensitivity', () => {

        it('converts 1H correctly', () => {

            const correctDuration = moment.duration({
                hours: 1
            });

            const duration = createDurationFromText('1H');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts 1H5M correctly', () => {

            const correctDuration = moment.duration({
                hours: 1,
                minutes: 5
            });

            const duration = createDurationFromText('1H5M');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts "1H  5M" correctly', () => {

            const correctDuration = moment.duration({
                hours: 1,
                minutes: 5
            });

            const duration = createDurationFromText('1H  5M');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts "1H  5   M" correctly', () => {

            const correctDuration = moment.duration({
                hours: 1,
                minutes: 5
            });

            const duration = createDurationFromText('1H  5   M');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts "1   H  5   M" correctly', () => {

            const correctDuration = moment.duration({
                hours: 1,
                minutes: 5
            });

            const duration = createDurationFromText('1   H  5   M');

            assert.deepEqual(duration, correctDuration);

        });

    });

    describe('not complete duration text', () => {

        it('converts "1" to "0 duration"', () => {

            const correctDuration = moment.duration({});

            const duration = createDurationFromText('1');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts "1H 5" to "1h"', () => {

            const correctDuration = moment.duration({
                hours: 1
            });

            const duration = createDurationFromText('1H 5');

            assert.deepEqual(duration, correctDuration);

        });

        it('converts "1H 10m 5" to "1h 10m"', () => {

            const correctDuration = moment.duration({
                hours: 1,
                minutes: 10
            });

            const duration = createDurationFromText('1H 10m 5');

            assert.deepEqual(duration, correctDuration);

        });

    });

});

describe('creating correct text string from moment duration', () => {

    describe('minutes text', () => {

        it('returns "5m" for 5m', () => {

            const momentDuration = moment.duration({minutes: 5});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '5m');

        });

        it('returns "10m" for 10m', () => {

            const momentDuration = moment.duration({minutes: 10});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '10m');

        });

        it('returns "15m" for 900s', () => {

            const momentDuration = moment.duration({seconds: 900});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '15m');

        });

    });

    describe('hours text', () => {

        it('returns 1h for "1h"', () => {

            const momentDuration = moment.duration({hours: 1});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '1h');

        });

        it('returns 3h for "3h"', () => {

            const momentDuration = moment.duration({hours: 3});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '3h');

        });

    });

    describe('hours & minutes text', () => {

        it('returns 1h5m for "1h 5m"', () => {

            const momentDuration = moment.duration({hours: 1});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '1h');

        });

        it('returns 3h5m for "3h 5m"', () => {

            const momentDuration = moment.duration({hours: 3, minutes: 5});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '3h 5m');

        });

    });

    describe('weeks & hours & minutes & seconds text', () => {

        it('returns 2w1h5m30s for "2w 1h 5m 30s"', () => {

            const momentDuration = moment.duration({weeks: 2, hours: 1, minutes: 5, seconds: 30});
            const textDuration = formatDuration(momentDuration);

            assert.equal(textDuration, '2w 1h 5m 30s');

        });

    });

    describe('invalid moment duration', () => {

        it('returns empty string', () => {

            const invalidMomentDuration = moment.duration.invalid();
            const textDuration = formatDuration(invalidMomentDuration);

            assert.equal(textDuration, '');

        });

    });

    describe('null passed intead of moment duration', () => {

        it('returns empty string', () => {

            const textDuration = formatDuration(null);

            assert.equal(textDuration, '');

        });

    });

});