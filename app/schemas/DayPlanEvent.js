const {placeSchema} = require('./Place');
const {taskSchema} = require('./Task');
const _ = require('lodash');
const mongoose = require('mongoose');

let planEventSchema = {
    _id: {type: String},
    title: {type: String},
    start: {type: Date},
    end: {type: Date},
    task: _.extend({}, {_id: mongoose.Schema.ObjectId}, taskSchema),
    location: placeSchema
};

module.exports = planEventSchema;