var userSchema = {
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    fullname: {type: String, required: true},
    token: {type: String},
    lastLogin: {type: Date}
};


module.exports = userSchema;
