const {placeSchema} = require('./Place');
const _ = require('lodash');

let timeSchema = {
    startDate: {type: Date},
    endDate: {type: Date},
    duration: {type: String},
    onDate: {type: Date}
};

let taskSchema = {
    title: {type: String, default: ''},
    description: {type: String, default: ''},
    location: placeSchema,
    time: timeSchema,
    category: {type: String, default: ''},
    priority: {type: Number},
    isUnspecified: {type: Boolean, default: false},
    isRegular: {type: Boolean, default: false},
    status: {type: String, default: 'notscheduled'},
};

let userTaskSchema = _.extend(
    {},
    taskSchema,
    {
        dayPlanIds: [String],
        userId: {type: String, required: true}
    }
);

// possible statuses: notscheduled -> scheduled -> inprogress -> done
// scheduled - added to a schedule of a certain day

module.exports = {
    taskSchema,
    userTaskSchema
};