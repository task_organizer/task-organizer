let planTravelBufferSchema = {
    _id:             {type: String},
    start:           {type: Date},
    end:             {type: Date},
    title:           {type: String},
    duration:        {type: String},
    travelMode:      {type: String},
    directions:      {type: Object},
    fromEventId:     {type: String},
    toEventId:       {type: String},
    departureBuffer: {type: Number},
    arrivalBuffer:   {type: Number}
};

module.exports = planTravelBufferSchema;