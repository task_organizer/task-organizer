const _ = require('lodash');
const mongoose = require('mongoose');

const dayPlanEventSchema = require('./DayPlanEvent');
const dayPlanTravelBufferSchema = require('./DayPlanTravelBuffer');
const {taskSchema} = require('./Task');

let DayPlanSchema = {
    userId:        {type: String, required: true},
    date:          {type: Date},
    events:        [dayPlanEventSchema],
    travelBuffers: [dayPlanTravelBufferSchema],
    todos:         [_.extend({}, {_id: mongoose.Schema.ObjectId}, taskSchema)]
};

module.exports = DayPlanSchema;