const _ = require('lodash');

var placeSchema = {
    name: {type: String, default: ''},
    loc: {
        type: {
            type: String,
            enum: 'Point',
            default: 'Point'
        },
        coordinates: [Number]
    },
    address: String,
    information: Object
};

var myPlaceSchema = _.extend({},
    placeSchema,
    {
        userId: {type: String, required: true}
    }
);

module.exports = {
    myPlaceSchema,
    placeSchema
};
