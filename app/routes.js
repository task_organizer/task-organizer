const express = require('express');
const path = require('path');
const PlacesController = require('./controllers/PlacesController');
const TasksController = require('./controllers/TasksController');
const CategoriesController = require('./controllers/CategoriesController');
const DayPlansController = require('./controllers/DayPlansController');
const AuthController = require('./controllers/AuthController');
const jwt = require('express-jwt');
const {privateKey} = require('./utils/enryption');
const {securityMiddleware} = require('./utils/securityMiddleware');
const requestDomain = require('./utils/requestDomain.js');
const setConnectionTimeout = require('./utils/setConnectionTimeout');

module.exports = function (app) {

    app.use(securityMiddleware);
    const initRequestDomain = function (req, res, next) {
        if (req.user) {
            requestDomain.put('user', req.user);
        } else {
            requestDomain.put('user', null);
        }
        next();
    };

    app.get('/', function (req, res) {
        res.render('index');
    });

    app.get('/partials/*', function (req, res) {
        const slicedPath = req.path.slice(1);
        const viewPath = slicedPath.slice(slicedPath.indexOf('/') + 1);
        const dotIndex = viewPath.lastIndexOf('.');
        const withoutExtension = viewPath.substr(0, dotIndex === -1 ? viewPath.length : dotIndex);
        res.render(withoutExtension);
    });

    app.post('/api/register', AuthController.register);
    app.post('/api/login', AuthController.login);

    app.get('/api/places', jwt({secret: privateKey}), initRequestDomain, PlacesController.getMyPlaces);
    app.put('/api/places/:id', jwt({secret: privateKey}), initRequestDomain, PlacesController.savePlace);
    app.post('/api/places', jwt({secret: privateKey}), PlacesController.addPlace);
    app.delete('/api/places/:id', jwt({secret: privateKey}), initRequestDomain, PlacesController.deletePlace);

    app.get('/api/tasks/:id', jwt({secret: privateKey}), initRequestDomain, TasksController.getTask);
    app.get('/api/tasks', jwt({secret: privateKey}), initRequestDomain, TasksController.getTasks);
    app.post('/api/tasks', jwt({secret: privateKey}), TasksController.addTask);
    app.put('/api/tasks/:id', jwt({secret: privateKey}), TasksController.saveTask);
    app.delete('/api/tasks/:id', jwt({secret: privateKey}), TasksController.deleteTask);

    app.get('/api/dayplans', jwt({secret: privateKey}), initRequestDomain, DayPlansController.getPlanForDay);
    app.post('/api/dayplans', setConnectionTimeout('2m'), jwt({secret: privateKey}), DayPlansController.createDayPlan);
    app.put('/api/dayplans/:id', setConnectionTimeout('2m'), jwt({secret: privateKey}), initRequestDomain, DayPlansController.updateDayPlan);
    app.put('/api/updateTodoInDayPlan/:id', jwt({secret: privateKey}), initRequestDomain, DayPlansController.updateTaskInDayPlan.bind({}, false));
    app.put('/api/updateEventInDayPlan/:id', jwt({secret: privateKey}), initRequestDomain, DayPlansController.updateTaskInDayPlan.bind({}, true));
    app.post('/api/addTaskAsTodoInDayPlan', jwt({secret: privateKey}), initRequestDomain, DayPlansController.addTaskAsTodoInDayPlan);

    app.get('/api/categories', jwt({secret: privateKey}), initRequestDomain, CategoriesController.getAllCategories);

    app.use('/js', express.static(path.join(__dirname, '../public/js')));
    app.use('/css', express.static(path.join(__dirname, '../public/css')));
    app.use('/fonts', express.static(path.join(__dirname, '../public/fonts')));
    app.use('/images', express.static(path.join(__dirname, '../public/images')));
    app.use('/node_modules', express.static(path.join(__dirname, '../node_modules')));

    app.get('/404', function (req, res) {
        res.send(404);
    });

    app.all('/*', function (req, res) {
        res.render('index');
    });

};