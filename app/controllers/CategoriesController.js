const Task = require('../models/Task');
const _ = require('lodash');

exports.getAllCategories = async (req, res) => {

    try {

        let categories = await Task.find({userId: req.user.id}).distinct('category');

        categories = _.filter(categories);

        res.send(categories);

    } catch (err) {

        res.status(400).send({message: err.message});

    }


};