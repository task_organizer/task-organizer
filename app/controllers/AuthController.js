const User = require('../models/User');
const {sign} = require('jsonwebtoken');
const Boom = require('boom');
const {privateKey, decrypt} = require('../utils/enryption');
const moment = require('moment');


exports.register = async (req, res) => {

    try {

        const user = await User.createUser(req.body);
        res.json(getUserData(user));
        await User.findByIdAndUpdate({_id: user._id}, {$set: {lastLogin: new Date}});

    } catch (err) {

        if (11000 === err.code || 11001 === err.code) {

            const response = Boom.forbidden('Please provide another user email');
            res.status(response.output.statusCode);
            return res.send(response);

        } else {

            const response = Boom.forbidden(err);
            res.status(response.output.statusCode); // HTTP 403
            return res.send(response);

        }

    }

};

exports.login = async function (req, res) {

    try {

        let user = await User.findUser(req.body.email);

        console.log(user);

        if (!user) {

            const response = Boom.forbidden('Invalid email or password');
            res.status(response.output.statusCode);
            return res.send(response);

        }

        if (req.body.password === decrypt(user.password)) {

            await User.findByIdAndUpdate({_id: user._id}, {$set: {lastLogin: new Date}});
            return res.json(getUserData(user));

        } else {

            const response = Boom.forbidden('Invalid email or password');
            res.status(response.output.statusCode);
            return res.send(response);

        }

    } catch (err) {

        if (11000 === err.code || 11001 === err.code) {

            const response = Boom.forbidden('Please provide another user email');
            res.status(response.output.statusCode);
            return res.send(response);

        } else {

            console.error(err);
            const response = Boom.badImplementation(err);
            res.status(response.output.statusCode);
            return res.send(response);

        }

    }

};

function createToken(user) {

    var tokenData = {
        email: user.email,
        id: user._id,
        exp: moment().add(14, 'days').unix()
    };

    var token = {
        email: user.email,
        token: sign(tokenData, privateKey)
    };

    return token;

}

function getUserData(user) {

    const token = createToken(user);

    return {
        token,
        _id: user._id,
        email: user.email,
        fullname: user.fullname,
        lastLogin: user.lastLogin
    }

}