const Place = require('./../models/Place');
const locationFormatter = require('./../utils/locationFormatter');
const _ = require('lodash');

exports.getMyPlaces = async (req, res) => {

    try {

        let places = await Place.find({}).lean().exec();

        if (places.length) {
            places.forEach((place) => {
                locationFormatter.prepareForClient(place);
            });
        }

        res.send(places);

    } catch (err) {

        res.status(400).send({message: err.message});

    }

};

exports.savePlace = async (req, res) => {

    const place = locationFormatter.prepareForDb(req.body);

    let newPlace = await Place.findByIdAndUpdate(req.params.id, place, {new: true});

    newPlace = locationFormatter.prepareForClient(newPlace);
    res.send(newPlace);

};

exports.addPlace = async (req, res) => {

    const place = _.extend({},
        locationFormatter.prepareForDb(req.body),
        {userId: req.user.id}
    );

    let newPlace = await Place.create(place);
    newPlace = newPlace.toObject();

    newPlace = locationFormatter.prepareForClient(newPlace);
    res.send(newPlace);

};

exports.deletePlace = async (req, res) => {

    await Place.findByIdAndRemove(req.params.id);
    res.status(200).send({});

};