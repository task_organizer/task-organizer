const Task = require('../models/Task');
const locationFormatter = require('../utils/locationFormatter');
const buildQuery = require('../utils/convertQueryParamsFromJsonToObjects');
const _ = require('lodash');
const Boom = require('boom');

exports.addTask = async (req, res) => {

    try {

        const task = _.extend({},
            locationFormatter.prepareTaskForDb(req.body),
            {userId: req.user.id}
        );

        let newTask = await Task.create(task);
        newTask = locationFormatter.prepareTaskForClient(newTask);
        res.status(200).send(newTask);

    } catch (err) {

        res.status(400).send({message: err.message});

    }

};

exports.getTasks = async (req, res) => {

    const sort = JSON.parse(req.query.sort || '{}');
    const recordsPerPage = Number(req.query.recordsPerPage);
    const page = Number(req.query.page) || 1;

    delete req.query.sort;
    delete req.query.recordsPerPage;
    delete req.query.page;

    const query = buildQuery(req.query);

    let queryOptions = {};

    if (recordsPerPage) {
        queryOptions = {
            skip: recordsPerPage * (page - 1),
            limit: recordsPerPage
        };
    }

    if (sort) {
        queryOptions = _.extend({},
            queryOptions,
            {sort}
        );
    }

    try {
        const tasks = await Task
            .find(query, [], queryOptions)
            .lean().exec();

        const coundQuery = _.extend({}, query, {userId: req.user.id});
        const totalCount = await Task.count(coundQuery);

        if (tasks.length) {
            tasks.forEach((task) => {
                locationFormatter.prepareTaskForClient(task);
            });
        }

        res.header('totalCount', totalCount);
        res.status(200).send(tasks);

    } catch (err) {

        res.status(400).send({message: err.message});

    }

};

exports.getTask = async (req, res) => {

    const taskId = req.params.id;

    const task = await Task.findById(taskId).lean().exec();

    if (!task) {
        const response = Boom.badRequest('Task does not exist');
        res.status(response.output.statusCode);
        return res.send(response);
    }

    try {

        locationFormatter.prepareTaskForClient(task);
        res.status(200).send(task);

    } catch (err) {

        res.status(400).send({message: err.message});

    }

};

exports.saveTask = async (req, res) => {

    const id = req.params.id;
    let updatedTask = req.body;

    try {

        updatedTask = _.extend({},
            locationFormatter.prepareTaskForDb(updatedTask),
            {userId: req.user.id}
        );

        let task = await Task.findByIdAndUpdate({_id: id}, {$set: updatedTask}, {new: true}).lean().exec();

        task = locationFormatter.prepareTaskForClient(task);
        res.status(200).send(task);

    } catch (err) {

        res.status(400).send({message: err.message});

    }

};

exports.deleteTask = async (req, res) => {

    const id = req.params.id;
    await Task.remove({_id: id});
    res.status(200).send();

};