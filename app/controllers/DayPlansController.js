const DayPlan = require('../models/DayPlan');
const Task = require('../models/Task');
const _ = require('lodash');
const ObjectId = require('mongoose').Types.ObjectId;
const Boom = require('boom');
const moment = require('moment');
const locationFormatter = require('../utils/locationFormatter');
const buildQuery = require('../utils/convertQueryParamsFromJsonToObjects');

exports.getPlanForDay = async (req, res) => {

    const query = buildQuery(req.query);

    try {

        const dayPlans = await DayPlan.find(query).lean().exec();

        if (dayPlans.length) {

            dayPlans.forEach((dayPlan) => {

                dayPlan.events = dayPlan.events || [];
                dayPlan.todos = dayPlan.todos || [];
                dayPlan.travelBuffers = dayPlan.travelBuffers || [];

                prepareDayPlanLocationsForClient(dayPlan);

            });

        }

        res.send(dayPlans);

    } catch (err) {

        res.status(400).send({message: err.message});

    }

};

exports.createDayPlan = async (req, res) => {

    const existingDayPlan = await getPlanForDay(req.body.date);

    if (existingDayPlan) {
        req.params.id = existingDayPlan._id;
        await exports.updateDayPlan(req, res);
        return;
    }

    const dayPlan = req.body;
    dayPlan.userId = req.user.id;

    prepareDayPlanLocationsForDb(dayPlan);
    const scheduledTasks = await markNewDayPlanTasksScheduled(dayPlan);

    try {

        cleanUserIdFromDayPlanTasks(dayPlan);
        let newDayPlan = await DayPlan.create(dayPlan).catch((err) => {
            console.error(err);
            throw new Error('not possible to save day plan');
        });

        await setupDayPlanIdForTasks(scheduledTasks, newDayPlan._id);

        prepareDayPlanLocationsForClient(newDayPlan);

        res.status(200).send(newDayPlan);

    } catch (err) {

        console.error(err);
        const response = Boom.internal('Day plan saving failed');
        res.status(response.output.statusCode);
        return res.send(response);

    }

};

exports.updateDayPlan = async (req, res) => {

    const dayPlanId = req.params.id;
    const newDayPlan = req.body;

    const oldDayPlan = await DayPlan.findById(dayPlanId).lean().exec();

    if (!oldDayPlan) {
        sendErrorResponse(res, 'Plan does not exist');
    }

    const scheduledTaskIds = await markNewDayPlanTasksScheduled(newDayPlan);
    const unscheduledTaskIds = await markRemovedDayPlanTasksNotDone(oldDayPlan, newDayPlan);

    removeDayPlanIdFromTasks(unscheduledTaskIds, dayPlanId);
    setupDayPlanIdForTasks(scheduledTaskIds, dayPlanId);

    prepareDayPlanLocationsForDb(newDayPlan);
    const updatedDayPlan = await DayPlan.findByIdAndUpdate({_id: dayPlanId}, {$set: newDayPlan}, {new: true}).lean().exec();

    prepareDayPlanLocationsForClient(updatedDayPlan);
    res.send(updatedDayPlan);

};

exports.updateTaskInDayPlan = async (updateEvent, req, res) => {

    const dayPlanId = req.params.id;
    const updatedTask = req.body;

    const dayPlan = await DayPlan.findById(dayPlanId).lean().exec();

    if (!dayPlan) {
        sendErrorResponse(res, 'Plan does not exist');
    }

    let dayPlanTasksCollectionToSearchIn = updateEvent ? dayPlan.events : dayPlan.todos;

    let taskInDayPlan = _.find(dayPlanTasksCollectionToSearchIn, (event) => event._id.toString() === updatedTask._id);

    if (taskInDayPlan) {

        _.extend(taskInDayPlan, updatedTask);
        prepareDayPlanLocationsForDb(dayPlan);

        const updatedDayPlan = await DayPlan.findByIdAndUpdate({_id: dayPlanId}, {$set: dayPlan}, {new: true}).lean().exec();

        Task.findByIdAndUpdate({_id: taskInDayPlan.task._id}, {$set: {status: taskInDayPlan.task.status}}).lean().exec();

        prepareDayPlanLocationsForClient(updatedDayPlan);

        let updatedTasksList = updateEvent ? updatedDayPlan.events : updatedDayPlan.todos;
        updatedTasksList = updatedTasksList || [];

        res.send(updatedTasksList);

    } else {
        sendErrorResponse(res, 'No task in the day plan found');
    }

};

exports.addTaskAsTodoInDayPlan = async (req, res) => {

    const date = req.body.date;
    const task = req.body.task;

    if (!date || !task) {
        sendErrorResponse(res, 'Not possible to add task in day plan: date or task is undefined');
        return;
    }

    const dayPlan = await getPlanForDay(date);

    if (dayPlan) {

        await DayPlan.update(
            {_id: dayPlan._id},
            {$push: {todos: task}}
        );

        res.status(200).send();

    } else {

        let dayPlan = {
            userId: req.user.id,
            date: date,
            todos: [task],
            events: [],
            travelBuffers: []
        };

        prepareDayPlanLocationsForDb(dayPlan);
        cleanUserIdFromDayPlanTasks(dayPlan);

        await DayPlan.create(dayPlan).catch((err) => {
            console.error(err);
            throw new Error('Not possible to save day plan');
        });

        res.status(200).send();

    }

    if (isTaskNotRegular(task)) {
        await updateTasksStatus([task._id], 'scheduled');
    }

};

function sendErrorResponse(res, err) {

    const response = Boom.badRequest(err);
    res.status(response.output.statusCode);
    return res.send(response);

}

function cleanUserIdFromDayPlanTasks(dayPlan) {

    const events = dayPlan.events;

    if (events && events.length) {

        dayPlan.events = _.map(events, (event) => {

            try {
                delete event.task.userId;
            } catch (err) {

            } finally {
                return event;
            }

        });
    }

    return dayPlan;

}

async function updateTasksStatus(taskIds, newStatus) {

    let definedTaskIds = _.filter(taskIds, (id) => id);
    let taskObjectIds = _.map(definedTaskIds, (id) => ObjectId(id));

    return await Task.update({_id: {$in: taskObjectIds}}, {$set: {status: newStatus}}, {multi: true});

}

async function setupDayPlanIdForTasks(taskIds, dayPlanId) {

    const taskObjectIds = _.map(taskIds, (taskId) => ObjectId(_.toString(taskId)));

    return await Task.update({
            _id: {$in: taskObjectIds},
            dayPlanIds: {$nin: [dayPlanId]}
        },
        {$push: {dayPlanIds: dayPlanId}},
        {multi: true}
    );

}

async function removeDayPlanIdFromTasks(taskIds, dayPlanId) {

    const taskObjectIds = _.map(taskIds, (taskId) => ObjectId(_.toString(taskId)));

    return await Task.update({
            _id: {$in: taskObjectIds},
            dayPlanIds: {$in: [dayPlanId]}
        },
        {$pull: {dayPlanIds: dayPlanId}},
        {multi: true}
    );

}

function prepareDayPlanLocationsForDb(dayPlan) {

    _.forEach(dayPlan.events, (dayPlanEvent) => {

        if (dayPlanEvent.location) {
            locationFormatter.prepareForDb(dayPlanEvent.location);
        }

        locationFormatter.prepareTaskForDb(dayPlanEvent.task);

    });
    _.forEach(dayPlan.todos, locationFormatter.prepareTaskForDb);

}

function prepareDayPlanLocationsForClient(dayPlan) {

    _.forEach(dayPlan.events, (dayPlanEvent) => {

        if (dayPlanEvent.location) {
            locationFormatter.prepareForClient(dayPlanEvent.location);
        }

        locationFormatter.prepareTaskForClient(dayPlanEvent.task)
    });
    _.forEach(dayPlan.todos, locationFormatter.prepareTaskForClient);

}

async function markNewDayPlanTasksScheduled(dayPlan) {

    const scheduledTaskIds = getTaskStringIdsForDayPlan(dayPlan);
    await updateTasksStatus(scheduledTaskIds, 'scheduled');

    dayPlan.events = _.map(dayPlan.events, (event) => {
        _.set(event, 'task.status', 'scheduled');
        return event;
    });

    return scheduledTaskIds;

}

async function markRemovedDayPlanTasksNotDone(oldDayPlan, newDayPlan) {

    const oldTaskIds = getTaskStringIdsForDayPlan(oldDayPlan);
    const newScheduledTaskIds = getTaskStringIdsForDayPlan(newDayPlan);

    let unscheduledTaskStringIds = [];

    if (oldTaskIds && newScheduledTaskIds) {

        unscheduledTaskStringIds = _.filter(oldTaskIds, (oldTaskId) => !_.includes(newScheduledTaskIds, oldTaskId));

    } else {

        unscheduledTaskStringIds = oldTaskIds;

    }

    await updateTasksStatus(unscheduledTaskStringIds, 'notscheduled');

    return unscheduledTaskStringIds;

}

function getTaskStringIdsForDayPlan(dayPlan) {

    const dayPlanEvents = _.get(dayPlan, 'events');
    const dayPlanTodos = _.get(dayPlan, 'todos');

    let dayPlanEventTasks = _.map(dayPlanEvents, (event) => _.get(event, 'task'));
    dayPlanEventTasks = _.filter(dayPlanEventTasks, isTaskNotRegular);
    let dayPlanEventTaskIds = _.map(dayPlanEventTasks, (task) => _.get(task, '_id'));
    dayPlanEventTaskIds = _.filter(dayPlanEventTaskIds);

    let notRegularTodos = _.filter(dayPlanTodos, isTaskNotRegular);
    const dayPlanTodoIds = _.filter(_.map(notRegularTodos, (todo) => todo._id));

    const dayPlanEventStringIds = _.map(dayPlanEventTaskIds, _.toString);
    const dayPlanTodoStringIds = _.map(dayPlanTodoIds, _.toString);

    return _.concat(dayPlanEventStringIds, dayPlanTodoStringIds);

}

function isTaskNotRegular(task) {
    return _.get(task, 'isRegular') !== true;
}

async function getPlanForDay(date) {

    const query = {
        date: {
            $gte: moment(date).startOf('day').toDate(),
            $lte: moment(date).endOf('day').toDate()
        }
    };

    const dayPlans = await DayPlan.find(query).lean().exec();
    if (dayPlans.length) {
        return _.first(dayPlans);
    }

}