var domain = require('domain');

module.exports = {
    put: function(key, value) {
        if (domain.active && domain.active.context) {
            domain.active.context[key] = value;
        } else {
            throw new Error('No active domain at this point!');
        }
    },

    get: function(key) {
        if (domain.active && domain.active.context) {
            return domain.active.context[key];
        } else {
            return undefined;
        }
    }
};