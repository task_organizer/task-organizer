const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const privateKey = '17LvDSfds3jYOh9Y';

function decrypt(password) {
    const decipher = crypto.createDecipher(algorithm, privateKey);
    let dec = decipher.update(password, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

// method to encrypt data(password)
function encrypt(password) {
    const cipher = crypto.createCipher(algorithm, privateKey);
    let crypted = cipher.update(password, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

module.exports = {
    decrypt,
    encrypt,
    privateKey
};