const moment = require('moment');

module.exports = function setConnectionTimeout(time) {
    var delay = typeof time === 'string'
        ? moment.duration(time).valueOf()
        : Number(time || 5000);

    return function (req, res, next) {
        res.connection.setTimeout(delay);
        next();
    }
};