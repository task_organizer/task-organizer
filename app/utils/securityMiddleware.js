const domain = require('domain');

const mongoose = require('mongoose');
const requestDomain = require('./requestDomain');
const Security = require('mongoose-model-security');
const security = new Security(mongoose);

// create the model provider
security.addModelProvider(function () {

    const user = requestDomain.get('user');

    return {
        user: user ? user : null
    };

});

module.exports = {

    securityMiddleware:   function middleware(req, res, next) {

        var reqDomain = domain.create();
        reqDomain.context = {};
        reqDomain.add(req);
        reqDomain.add(res);

        res.on('close', function () {
            reqDomain.exit();
        });

        res.on('finish', function () {
            reqDomain.exit();
        });

        reqDomain.on('error', function () {
            // just make sure the domain is exited
            reqDomain.exit();
        });

        reqDomain.run(function () {
            next();
        });

    },

    userSecurity: security
};