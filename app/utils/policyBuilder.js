const {userSecurity} = require('./securityMiddleware');

module.exports = {

    buildReadUserIdPolicyForModelName:   (name, allowAccess) => {

        userSecurity.buildPolicy(name).read(allowAccess || function (parameters) {

            const userId = parameters.user ? parameters.user.id : null;

            if (userId) {
                return {userId};
            } else {
                return false;
            }

        });

    },
    buildUpdateUserIdPolicyForModelName: (name) => {

        userSecurity.buildPolicy(name).update(function (parameters) {

            const userId = parameters.user ? parameters.user.id : null;

            if (userId) {
                return {userId};
            } else {
                return false;
            }

        });

    },
    buildDeleteUserIdPolicyForModelName: (name) => {

        userSecurity.buildPolicy(name).remove(function (parameters) {

            const userId = parameters.user ? parameters.user.id : null;

            if (userId) {
                return {userId};
            } else {
                return false;
            }

        });

    },
    buildCreateUserIdPolicyForModelName: (name) => {

        userSecurity.buildPolicy(name).create(true);

    },

};

