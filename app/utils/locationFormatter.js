function prepareForClient(location) {

    location.longitude = location.loc.coordinates[0];
    location.latitude = location.loc.coordinates[1];
    delete location.loc;

    return location;

}

function prepareForDb(location) {

    const arePlaceCoordinatesNotDefined = location.longitude !== 0 && location.longitude == undefined && location.latitude !== 0 && location.latitude == undefined;

    if (!arePlaceCoordinatesNotDefined) {
        const loc = {
            type:        'Point',
            coordinates: [location.longitude, location.latitude]
        };

        location.loc = loc;
    }

    return location;

}

function prepareTaskForClient(task) {

    if (task.location) {

        prepareForClient(task.location);

    }

    return task;

}

function prepareTaskForDb(task) {

    if (task.location) {

        prepareForDb(task.location);

    }

    return task;

}

module.exports.prepareForClient = prepareForClient;
module.exports.prepareForDb = prepareForDb;
module.exports.prepareTaskForDb = prepareTaskForDb;
module.exports.prepareTaskForClient = prepareTaskForClient;