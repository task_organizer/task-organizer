const _ = require('lodash');


module.exports = function convertJSONPropsToObjects(queryObject) {

    let query = _.isArray(queryObject) ? [] : {};

    if(!queryObject) {
        return query;
    }

    for(let prop in queryObject) {

        const propValue = queryObject[prop];

        if(_.isString(propValue)) {

            try {

                query[prop] = JSON.parse(propValue);

            } catch(err) {

                query[prop] = propValue;

            }

        } else if(_.isObject(propValue)) {

            query[prop] = convertJSONPropsToObjects(propValue);

        } else {

            query[prop] = propValue;

        }

    }

    return query;

};