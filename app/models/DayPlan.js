const mongoose = require('mongoose');
const dayPlanStructure = require('../schemas/DayPlan');
const {buildCreateUserIdPolicyForModelName, buildDeleteUserIdPolicyForModelName, buildReadUserIdPolicyForModelName, buildUpdateUserIdPolicyForModelName} = require('../utils/policyBuilder');

let dayPlanSchema = new mongoose.Schema(dayPlanStructure);
let dayPlanModel = mongoose.model('DayPlan', dayPlanSchema, 'DayPlans');

buildCreateUserIdPolicyForModelName('DayPlan');
buildReadUserIdPolicyForModelName('DayPlan');
buildUpdateUserIdPolicyForModelName('DayPlan');
buildDeleteUserIdPolicyForModelName('DayPlan');

module.exports = dayPlanModel;
