// grab the mongoose module
const mongoose = require('mongoose');
const {myPlaceSchema} = require('../schemas/Place');
const {buildCreateUserIdPolicyForModelName, buildDeleteUserIdPolicyForModelName, buildReadUserIdPolicyForModelName, buildUpdateUserIdPolicyForModelName} = require('../utils/policyBuilder');

let placeMongooseSchema = new mongoose.Schema(myPlaceSchema);

buildCreateUserIdPolicyForModelName('Place');
buildReadUserIdPolicyForModelName('Place');
buildUpdateUserIdPolicyForModelName('Place');
buildDeleteUserIdPolicyForModelName('Place');

module.exports = mongoose.model('Place', placeMongooseSchema, 'Places');
