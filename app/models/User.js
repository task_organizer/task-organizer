// grab the mongoose module
const mongoose = require('mongoose');
const userStructure = require('../schemas/User');
const {encrypt} = require('../utils/enryption');
const {buildCreateUserIdPolicyForModelName, buildReadUserIdPolicyForModelName} = require('../utils/policyBuilder');


let userSchema = new mongoose.Schema(userStructure);

buildCreateUserIdPolicyForModelName('User');
buildReadUserIdPolicyForModelName('User', true);

userSchema.statics.createUser = async function (user) {

    user.password = encrypt(user.password);
    return this.create(user);

};

userSchema.statics.findUser = async function(email) {

    return await this.findOne({email});

};

module.exports = mongoose.model('User', userSchema, 'Users');
