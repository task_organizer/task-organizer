const {buildCreateUserIdPolicyForModelName, buildDeleteUserIdPolicyForModelName, buildReadUserIdPolicyForModelName, buildUpdateUserIdPolicyForModelName} = require('../utils/policyBuilder');

const mongoose = require('mongoose');
const {userTaskSchema} = require('../schemas/Task');

buildCreateUserIdPolicyForModelName('Task');
buildReadUserIdPolicyForModelName('Task');
buildUpdateUserIdPolicyForModelName('Task');
buildDeleteUserIdPolicyForModelName('Task');

const mongooseTaskSchema = new mongoose.Schema(userTaskSchema);

module.exports = mongoose.model('Task', mongooseTaskSchema, 'Tasks');
