(function () {
    'use strict';

    angular.module('organizer.tasks').controller('tasksCtrl', [
        '$scope',
        '$location',
        'NgTableParams',
        'tasksManager',
        'tasksFilterBuilder',
        'tasksStatusFilter',
        tasksCtrl
    ]);

    function tasksCtrl($scope, $location, NgTableParams, tasksManager, tasksFilterBuilder, tasksStatusFilter) {

        $scope.taskStatuses = tasksStatusFilter;
        const requestQueryParameters = getSearchParameters($location);

        $scope.tableParams = new NgTableParams({
                filter: requestQueryParameters.filter,
                sorting: requestQueryParameters.sort,
                count: requestQueryParameters.recordsPerPage,
                page: requestQueryParameters.page
            },
            {
                getData: function (params) {

                    let routeSearchParameters = {
                        ...params.filter(),
                        sort: params.sorting() ? JSON.stringify(params.sorting()) : undefined,
                        recordsPerPage: params.count(),
                        page: params.page()
                    };
                    $location.search(routeSearchParameters);

                    let filter = params.filter();
                    filter = handleStatusFilter(filter);
                    filter = handleLocationFilter(filter);

                    let query = tasksFilterBuilder.transformSearchParamsToRegexpQuery(filter,
                        (str) => _.isString(str) ?
                            _.map(str.split(','), _.trim) :
                            str
                    );

                    query = {
                        ...query,
                        sort: processSortOptions(params.sorting())
                    };

                    const page = params.page();
                    const recordsPerPage = params.count();

                    query = {
                        ...query,
                        page,
                        recordsPerPage
                    };

                    return tasksManager.getTasks(query);

                }
            }
        );

        $scope.$on('refreshTasks', () => {
            $scope.tableParams.reload();
        });

        $scope.deleteTask = async (task) => {
            await tasksManager.deleteTask(task);
            $scope.tableParams.reload();
        };

        function processSortOptions(sort) {

            let sortingOptions = {};

            _.each(_.keys(sort), (sortingKey) => {
                sortingOptions[sortingKey] = sort[sortingKey] === 'asc' ? 1 : -1;
            });

            return sortingOptions;

        }

        function handleStatusFilter(query) {

            if (query.status === 'regular') {

                query = {
                    ...query,
                    isRegular: true,
                };

                delete query.status;
            }

            return query;
        }

        function handleLocationFilter(query) {

            if (query.location) {

                query = {
                    ...query,
                    $or: [
                        {
                            'location.name': query.location
                        },
                        {
                            'location.information': query.location
                        },
                        {
                            'location.address': query.location
                        }
                    ]
                };

                delete query.location;

            }

            return query;

        }

        function getSearchParameters($location) {

            let urlQuery = $location.search();

            const sort = _.isString(urlQuery.sort) ? JSON.parse(urlQuery.sort) : undefined;
            const page = urlQuery.page || 1;
            const recordsPerPage = urlQuery.recordsPerPage || 10;

            delete urlQuery.sort;
            delete urlQuery.page;
            delete urlQuery.recordsPerPage;

            return {
                filter: urlQuery,
                sort,
                page,
                recordsPerPage
            };

        }

    }

})();
