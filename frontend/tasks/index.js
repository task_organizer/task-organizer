(function() {
    'use strict';

    require('./../common/services');
    require('./../common/directives');

    angular.module('organizer.tasks', ['organizer.common.services', 'organizer.common.directives']);

    require('./controller');


})();