(function () {
    'use strict';

    angular.module('organizer.tasks').controller('newTaskCtrl', [
        '$scope',
        'myPlaces',
        tasksCtrl]);

    function tasksCtrl($scope, myPlaces) {

        $scope.isAdvancedView = true;
        $scope.myPlaces = myPlaces;

    }

})();
