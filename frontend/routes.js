'use strict';


angular.module('organizer').config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('login', {
                url: '/login',
                title: 'Login',
                templateUrl: '/partials/login/view.html',
                controller: 'loginCtrl'
            })
            .state('registration', {
                url: '/registration',
                title: 'Registration',
                templateUrl: '/partials/registration/view.html',
                controller: 'registrationCtrl'
            })
            .state('authorized', {
                templateUrl: '/partials/common/views/authorized.html'
            })
            .state('newTask', {
                url: '/tasks/new',
                title: 'New Task',
                templateUrl: '/partials/new-task/view.html',
                controller: 'newTaskCtrl',
                parent: 'authorized',
                resolve: {
                    myPlaces: [
                        'placesService', function (placesService) {

                            return placesService.getMyPlaces();

                        }
                    ]
                }
            })
            .state('planning', {
                url: '/planning/:date',
                title: 'Plan your day',
                templateUrl: '/partials/planning/view',
                controller: 'planningCtrl',
                parent: 'authorized',
                resolve: {
                    dayPlan: ['$stateParams', 'dayPlanManager', function ($stateParams, dayPlanManager) {

                        let dateParam = $stateParams.date;
                        let date = Date.parse(dateParam);

                        return dayPlanManager.getDayPlan(date);

                    }],
                    tasksAssignedToDay: ['$stateParams', 'tasksManager', ($stateParams, tasksManager) => {

                        let dateParam = $stateParams.date;
                        let date = Date.parse(dateParam);

                        return tasksManager.getNotscheduledTasksWithAssignedDay(date);

                    }],
                    tasksFilter: ['$stateParams', async ($stateParams) => {

                        // const day = Date.parse($stateParams.date);
                        // const startDate = moment(day).startOf('day');
                        // const endDate = moment(day).endOf('week');

                        return {};

                        // return {
                        //     'time.startDate': new Date(startDate),
                        //     'time.endDate': new Date(endDate)
                        // };

                    }],
                    upcomingTasks: ['tasksManager', function (tasksManager) {

                        return tasksManager.getTasks();
                        // let dateParam = $stateParams.date;
                        // let date = Date.parse(dateParam);
                        //
                        // return tasksManager.getTasksForWeek(date);

                    }],
                    selectedDate: ['$q', '$stateParams', function ($q, $stateParams) {

                        let dateParam = $stateParams.date;
                        return Date.parse(dateParam);

                    }]
                }
            })
            .state('dashboard', {
                title: 'Dashboard',
                url: '/:date',
                parent: 'authorized',
                templateUrl: '/partials/dashboard/view.html',
                controller: 'mainCtrl',
                resolve: {
                    dayPlan: [
                        '$stateParams', 'dayPlanManager', function ($stateParams, dayPlanManager) {

                            let dateParam = $stateParams.date;
                            let date = Date.parse(dateParam);

                            if (!date || isNaN(date.getTime())) {
                                date = new Date();
                            }

                            return dayPlanManager.getDayPlan(date);

                        }
                    ],
                    selectedDate: ['$stateParams', function ($stateParams) {

                        let dateParam = $stateParams.date;
                        let date = Date.parse(dateParam);

                        if (!date || isNaN(date.getTime())) {
                            date = new Date();
                        }

                        return date;

                    }],
                    isDateInPast: ['$stateParams', 'datesManager', ($stateParams, datesManager) => {
                        let dateParam = $stateParams.date;
                        return datesManager.isDateInPast(dateParam);
                    }]
                }
            })
            .state('dashboardToday', {
                title: 'Dashboard',
                url: '/',
                parent: 'authorized',
                templateUrl: '/partials/dashboard/view.html',
                controller: 'mainCtrl',
                resolve: {
                    dayPlan: [
                        'dayPlanManager', function (dayPlanManager) {

                            return dayPlanManager.getDayPlan(new Date());

                        }
                    ],
                    selectedDate: function () {

                        return new Date();

                    },
                    isDateInPast: () => false
                }
            })
            .state('tasks', {
                title: 'My Tasks',
                url: '/tasks?sort&recordsPerPage&page&category',
                reloadOnSearch: false,
                parent: 'authorized',
                templateUrl: '/partials/tasks/view.html',
                controller: 'tasksCtrl',
                resolve: {}
            })
            .state('details', {
                title: 'Task Details',
                url: '/:id?sort&recordsPerPage&page',
                parent: 'tasks',
                templateUrl: '/partials/task-details/view.html',
                controller: 'taskDetailsCtrl',
                resolve: {
                    task: ['$stateParams', 'tasksManager', function ($stateParams, tasksManager) {

                        return tasksManager.getTaskById($stateParams.id);

                    }],
                    myPlaces: ['placesService', function (placesService) {

                        return placesService.getMyPlaces();

                    }],
                    categories: ['categoriesManager', async function (categoriesManager) {

                        const categories = await categoriesManager.getAllCategories();
                        return _.map(categories, (category) => {
                            return {name: category}
                        });

                    }]
                }
            })
            .state('places', {
                url: '/places',
                title: 'My places',
                templateUrl: '/partials/places/view',
                controller: 'placesCtrl',
                parent: 'authorized',
                resolve: {
                    myPlaces: ['placesService', function (placesService) {

                        return placesService.getMyPlaces();

                    }]
                }
            });

        // $routeProvider
        //
        //     .when('/tasks/new', {
        //         title:       'New task',
        //         templateUrl: '/partials/new-task/view',
        //         controller:  'newTaskCtrl',
        //         resolve:     {
        //             myPlaces: [
        //                 'placesService', function (placesService) {
        //
        //                     return placesService.getMyPlaces();
        //
        //                 }
        //             ]
        //         }
        //     })
        //
        //     .when('/places', {
        //         title:       'My places',
        //         templateUrl: '/partials/places/view',
        //         controller:  'placesCtrl',
        //         resolve:     {
        //             myPlaces: [
        //                 'placesService', function (placesService) {
        //
        //                     return placesService.getMyPlaces();
        //
        //                 }
        //             ]
        //         }
        //     })
        //
        //     .when('/planning/:date', {
        //         title:       'Plan your day',
        //         templateUrl: '/partials/planning/view',
        //         controller:  'planningCtrl',
        //         resolve:     {
        //             dayPlan:       [
        //                 '$route', 'planningManager', function ($route, planningManager) {
        //
        //                     let dateParam = $route.current.params.date;
        //                     let date = Date.parse(dateParam);
        //
        //                     return planningManager.getDayPlan(date);
        //
        //                 }
        //             ],
        //             upcomingTasks: [
        //                 'tasksManager', function (tasksManager) {
        //
        //                     return tasksManager.getTasksForWeek(moment());
        //
        //                 }
        //             ],
        //             selectedDate:  [
        //                 '$route', '$q', function ($route, $q) {
        //
        //                     let dateParam = $route.current.params.date;
        //                     let date = Date.parse(dateParam);
        //
        //                     let deferred = $q.defer();
        //                     deferred.resolve(date);
        //                     return deferred.promise;
        //                 }
        //             ]
        //         }
        //     })
        //
        //     .when('/login', {
        //         title:       'Login',
        //         templateUrl: '/partials/login/view',
        //         controller:  'loginCtrl'
        //     })
        //
        //     .when('/', {
        //         title:       'Home',
        //         templateUrl: '/partials/homepage/view',
        //         controller:  'mainCtrl'
        //     })
        //
        //     .otherwise({
        //         redirectTo: '/404'
        //     });

        // $locationProvider.html5Mode(true);

    }
]);