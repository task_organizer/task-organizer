(function () {
    'use strict';

    angular.module('organizer.places').controller('placesCtrl', [
        '$scope',
        'NgMap',
        'toastr',
        'placesService',
        'myPlaces',
        placesCtrl
    ]);

    function placesCtrl($scope, NgMap, toastr, placesService, myPlaces) {

        $scope.places = myPlaces;
        // jumping location - when hover the location list
        $scope.placeToHighlight = {};
        // when hovering the marker to hover the location in the list - not jumping
        $scope.selectedPlace = {};
        $scope.placeToEdit = {};
        $scope.isInEditMode = false;
        $scope.placeToDelete = {};
        $scope.mapId = 'my-places-map';
        $scope.mapControl = null;


        $scope.highlightPlace = function (place) {

            if (!$scope.isInEditMode) {
                $scope.placeToHighlight = place;
                $scope.selectedPlace = place;
            }

        };

        $scope.unhighlightPlace = function () {

            if (!$scope.isInEditMode) {

                $scope.placeToHighlight = null;
                $scope.selectedPlace = {};

            }

        };

        $scope.zoomToMarker = async function (place) {

            $scope.mapControl.setCenter({lat: place.latitude, lng: place.longitude});
            $scope.mapControl.setZoom(15);

        };

        $scope.editPlace = function (place) {

            $scope.$emit('removeSelectionFromLocation');
            $scope.isInEditMode = true;
            $scope.placeToEdit = place;

            $scope.$$postDigest(async function () {

                await handleMapResize();
                await $scope.zoomToMarker(place);

            });

        };

        $scope.savePlace = function (place) {

            var successCallback = function () {

                toastr.success('Place was successfully saved');
                $scope.$emit('stopEditingLocation');

            };

            var errorCallback = function () {

                toastr.error('An error occurred while saving');

            };

            if (place._id) {

                placesService.savePlace(place).then(successCallback, errorCallback);

            } else {

                placesService.addNewPlace(place).then(successCallback, errorCallback);

            }

        };

        $scope.addPlace = async function () {

            if ($scope.isInEditMode) {
                toastr.warning('Please save or cancel the currently edited place');
                return;
            }

            const newPlace = {};
            const center = $scope.mapControl.getCenter();

            newPlace.latitude = center.lat();
            newPlace.longitude = center.lng();
            $scope.places.push(newPlace);
            $scope.editPlace(newPlace);

        };

        $scope.deletePlace = function (place) {

            placesService.deletePlace(place).then(function () {

                $scope.unhighlightPlace();

            });

        };

        $scope.attemptToDelete = function (place) {

            $scope.placeToDelete = place;

        };

        $scope.declineDelete = function () {

            $scope.placeToDelete = {};

        };

        $scope.$on('removeSelectionFromLocation', function () {

            $scope.selectedPlace = {};

        });

        $scope.$on('stopEditingLocation', function () {

            $scope.selectedPlace = {};
            $scope.placeToEdit = {};
            $scope.isInEditMode = false;
            $scope.unhighlightPlace();

            handleMapResize();

        });

        function handleMapResize() {

            return new Promise((resolve) => {

                setTimeout(() => {
                    google.maps.event.trigger($scope.mapControl, 'resize');
                    resolve();
                }, 200);

            });

        }

        NgMap.getMap($scope.mapId).then((map) => {
            $scope.mapControl = map;
        });

    }

})();
