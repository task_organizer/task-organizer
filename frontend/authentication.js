const app = angular.module('organizer');

app.run([
    '$state', '$transitions', '$uibModal', 'authManager', 'jwtHelper', '$rootScope', 'toastr',
    function ($state, $transitions, $uibModal, authManager, jwtHelper, $rootScope, toastr) {

        $transitions.onStart({}, function (transition) {

            const to = transition.$to();
            const parentName = _.get(to, 'parent.name');
            const tokenObject = authManager.getToken();

            if (parentName === 'authorized') {

                try {

                    if (!tokenObject || !tokenObject.token) {
                        throw new Error('no token, redirect to login');
                    }

                    const tokenExpired = jwtHelper.isTokenExpired(tokenObject.token);

                    if (tokenExpired) {
                        throw new Error('token expired, redirect to login');
                    }

                    if (shouldShowNewUserInstructions(transition.$from(), authManager)) {
                        showFirstLoginInstructionsModal($uibModal);
                    }

                } catch (error) {
                    return transition.router.stateService.target('login');
                }

            } else if (to.name === 'login' || to.name === 'registration') {

                if (tokenObject && tokenObject.token) {
                    const tokenExpired = jwtHelper.isTokenExpired(tokenObject.token);

                    if (!tokenExpired) {
                        return transition.router.stateService.target('dashboard');
                    }
                }
            }

        });

        $rootScope.$on('tokenHasExpired', function () {

            $location.path('/login');
            toastr.error('Session has expired!');

        });

        function shouldShowNewUserInstructions(transitionFrom, authManager) {

            if (transitionFrom.name === 'login' || transitionFrom.name === 'registration') {

                return _.isUndefined(authManager.getLastLogin());

            }

        }

        function showFirstLoginInstructionsModal($uibModal) {

            $uibModal.open({
                size: 'md',
                windowClass: 'first-instructions-modal',
                controller: ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
                    $scope.closeModal = () => {$uibModalInstance.close()};
                }],
                templateUrl: '/partials/common/views/first-login-instructions'
            });

        }

    }
]);

// this configuration attaches JSON web token to each request sent to the server
// whiteListedDomains is important, because it constraints the list of urls, where the requests will be sent with attached JSON web token
app.config([
    '$httpProvider', 'jwtOptionsProvider', function Config($httpProvider, jwtOptionsProvider) {

        jwtOptionsProvider.config({

            tokenGetter: [
                'authManager', function (authManager) {

                    const tokenObj = authManager.getToken();
                    return _.get(tokenObj, 'token');

                }
            ],
            whiteListedDomains: ['localhost', 'smart-planning.herokuapp.com']

        });

        $httpProvider.interceptors.push('jwtInterceptor');
    }
]);