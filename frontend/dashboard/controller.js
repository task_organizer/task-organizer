(function () {
    'use strict';

    angular.module('organizer.dashboard').controller('mainCtrl', [
        '$scope', '$location', 'dayPlanManager', 'tasksFilterBuilder',
        'dayPlan', 'selectedDate', 'isDateInPast', mainController
    ]);

    function mainController($scope, $location, dayPlanManager, tasksFilterBuilder, dayPlan, selectedDate, isDateInPast) {

        $scope.dayPlan = dayPlan;
        $scope.selectedDate = selectedDate;
        $scope.debouncedTaskSearchQuery = tasksFilterBuilder.getCriticalNotScheduledTasksFilter();
        $scope.selectedDateEditLink = moment(selectedDate).format('D-MMM-Y');
        $scope.isDateInPast = isDateInPast;
        $scope.formattedDate = moment(selectedDate).format('dddd, D MMMM, Y');
        $scope.dayPlanHasItems = $scope.dayPlan.events.length || $scope.dayPlan.todos.length;

        $scope.newDateForDashboardSet = (newDate) => {

            const dateValue = moment(newDate).format('D-MMM-Y');
            $location.url('/' + dateValue);

        };

        $scope.todoTaskStatusUpdated = async (task) => {

            $scope.dayPlan.todos = await dayPlanManager.updateTodoInDayPlan($scope.dayPlan, task);
            $scope.$apply();

        };

        $scope.eventTaskStatusUpdated = async (event) => {

            $scope.dayPlan.events = await dayPlanManager.updateEventInDayPlan($scope.dayPlan, event);
            $scope.$apply();

        };

        $scope.$watch('taskSearchQuery', (newValue) => {

            if (newValue && newValue.length > 2) {

                $scope.debouncedTaskSearchQuery = tasksFilterBuilder.buildFilterFromSearchString(newValue);

            } else {

                $scope.debouncedTaskSearchQuery = tasksFilterBuilder.getCriticalNotScheduledTasksFilter();

            }

        });

    }

})();
