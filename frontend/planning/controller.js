(function () {
    'use strict';

    angular.module('organizer.planning').controller('planningCtrl', [
        '$scope',
        '$rootScope',
        '$location',
        '$interval',
        'toastr',
        'planningDayCalendarConfig',
        'defaultPlanEventDuration',
        'tasksFilterBuilder',
        'tasksManager',
        'planningManager',
        'dayPlanManager',
        'datesManager',
        'selectedDate',
        'dayPlan',
        'upcomingTasks',
        'tasksAssignedToDay',
        'tasksFilter',
        planningCtrl
    ]);

    function planningCtrl($scope, $rootScope, $location, $interval, toastr,
                          planningDayCalendarConfig, defaultPlanEventDuration, tasksFilterBuilder,
                          tasksManager, planningManager, dayPlanManager, datesManager,
                          selectedDate, dayPlan, upcomingTasks, tasksAssignedToDay, tasksFilter) {

        $scope.planningDate = selectedDate;
        $scope.formattedDate = moment(selectedDate).format('dddd, D MMMM, Y');
        $scope.defaultPlanEventDuration = defaultPlanEventDuration;

        $scope.dayPlan = dayPlan;
        $scope.tasks = upcomingTasks;
        $scope.tasksAssignedToDay = tasksAssignedToDay;
        $scope.tasksFilter = tasksFilter;

        $scope.dayPlanCalendarConfig = {
            ...planningDayCalendarConfig,
            eventStartEditable: true,
            eventDurationEditable: true,
            droppable: true,
            height: 'parent'
        };
        $scope.routeSelectionActive = false;

        $scope.routesForSelectionQueue = [];

        $scope.displayMode = 'schedule';

        $scope.dayPlanSaveState = 'Automatic saving enabled';
        $scope.savedTimeStamp = null;
        $scope.savedStateInterval = null;

        let tasksToExecuteAfterSavingDayPlan = [];
        initSavedStateInterval();

        $scope.changeDisplayMode = (newDisplayMode) => {
            $scope.displayMode = newDisplayMode;
        };

        $scope.newPlanningDateWasSet = function (newDate) {

            const dateValue = moment(newDate).format('D-MMM-Y');
            $location.url('/planning/' + dateValue);

        };

        $scope.updateSearchResults = async function () {

            let query = tasksFilterBuilder.transformSearchParamsToRegexpQuery($scope.tasksFilter);
            tasksManager.getTasks(query).then((tasks) => {
                $scope.tasks = tasks;
            });

            $scope.tasksAssignedToDay = await tasksManager.getNotscheduledTasksWithAssignedDay($scope.planningDate);
            $scope.$apply();

        };

        $scope.saveDayPlanClick = async function (dayPlan) {

            try {

                $scope.dayPlan = await saveDayPlan(dayPlan);
                $scope.updateSearchResults();
                toastr.success('Day plan was saved');

            } catch (err) {

                toastr.error('Error occurred while saving');

            }

        };

        $scope.taskDroppedInTodoList = function (taskId) {

            const droppedTask = _.find($scope.tasks, {_id: taskId});
            const taskAlreadyInTodo = _.some($scope.dayPlan.todos, (todo) => todo._id === taskId);

            if (droppedTask && !taskAlreadyInTodo) {

                const plainTaskObject = _.isFunction(droppedTask.plain) && droppedTask.plain();
                $scope.dayPlan.todos.push(plainTaskObject || droppedTask);
                updateTaskStatus(droppedTask, 'scheduled');
                saveDayPlan($scope.dayPlan);

            }

        };

        $scope.addScheduledTaskInTodoList = async function (task) {
            if (task) {
                $scope.taskDroppedInTodoList(task._id);
                $scope.tasksAssignedToDay = _.filter($scope.tasksAssignedToDay, (scheduledTask) => scheduledTask !== task);

                tasksToExecuteAfterSavingDayPlan.push(async () => {
                    $scope.tasksAssignedToDay = await tasksManager.getNotscheduledTasksWithAssignedDay($scope.planningDate)
                });
            }
        };

        $scope.addScheduledTaskInDaySchedule = async function (task) {

            updateTaskStatus(_.find($scope.tasks, (searchResult) => searchResult._id === task._id), 'scheduled');

            const startDate = moment(task.time.onDate);
            const duration = moment.duration(task.time.duration || defaultPlanEventDuration);
            const endDate = moment(startDate).add(duration);
            const event = dayPlanManager.createDayPlanEventFromTask(task, startDate, endDate);

            $scope.dayPlan.events.push(event);
            $scope.eventAddedToDayPlan(event);

            tasksToExecuteAfterSavingDayPlan.push(async () => {
                $scope.tasksAssignedToDay = await tasksManager.getNotscheduledTasksWithAssignedDay($scope.planningDate);
                $scope.$apply();
            });

        };

        $scope.onDropToUnschedule = function (taskId) {

            if (taskId) {
                _.remove($scope.dayPlan.todos, (todo) => todo && todo._id === taskId);

                const taskInSearchResults = _.find($scope.tasks, {_id: taskId});
                if(taskInSearchResults) {
                    updateTaskStatus(taskInSearchResults, 'notscheduled');
                }
                saveDayPlan($scope.dayPlan);
            }

        };

        $scope.removeTaskFromTodos = function (task) {
            if (task) {
                $scope.onDropToUnschedule(task._id);
            }
        };

        $scope.onEventDurationChanged = async function (event, oldEndTime) {

            const reschedulingParameters = {
                eventOldEndTime: oldEndTime
            };

            await planningManager.updateTravelBuffersForShiftedEvent(event, $scope.dayPlan.travelBuffers, reschedulingParameters);

            $scope.$digest();
            saveDayPlan($scope.dayPlan);

        };

        $scope.onEventRescheduled = async function (event, oldStartTime, oldEndTime) {

            if (planningManager.isEventOrderChangedAfterRescheduling($scope.dayPlan.events, event, oldStartTime, oldEndTime)) {

                $scope.routesForSelectionQueue = await planningManager.getRescheduledTravelBufferAfterReorderingEvents($scope.dayPlan, event, oldStartTime, oldEndTime);

                if (shouldShowRouteSelection()) {
                    $scope.routeForSelection = _.first($scope.routesForSelectionQueue);
                    $scope.routeSelectionActive = true;
                    $scope.$apply();
                }

            } else {

                const reschedulingParameters = {
                    eventOldStartTime: oldStartTime,
                    eventOldEndTime: oldEndTime
                };
                await planningManager.updateTravelBuffersForShiftedEvent(event, $scope.dayPlan.travelBuffers, reschedulingParameters);

                saveDayPlan($scope.dayPlan);

            }

        };

        $scope.onEventUnscheduled = async function (event) {

            const travelBuffersToDelete = planningManager.getTravelBuffersForUnscheduledEvent(event, $scope.dayPlan.travelBuffers);
            _.remove($scope.dayPlan.travelBuffers, (tb) => _.some(travelBuffersToDelete, tb));

            const siblingEvents = planningManager.getSiblingEventsWithLocations(event, $scope.dayPlan.events);
            _.remove($scope.dayPlan.events, event);

            if (siblingEvents.nextEvent && siblingEvents.previousEvent && planningManager.shouldHaveTravelBuffer(siblingEvents.previousEvent, siblingEvents.nextEvent)) {

                const route = {
                    fromEvent: siblingEvents.previousEvent,
                    toEvent: siblingEvents.nextEvent
                };

                $scope.routesForSelectionQueue.push(route);
                $scope.routeForSelection = route;
                $scope.routeSelectionActive = true;

            } else {
                saveDayPlan($scope.dayPlan);
                $scope.tasksAssignedToDay = await tasksManager.getNotscheduledTasksWithAssignedDay($scope.planningDate);
            }

        };

        $scope.eventAddedToDayPlan = async function (event) {

            $scope.routesForSelectionQueue = await planningManager.getRoutesForSelection(event, $scope.dayPlan);

            const existingTravelBuffer = planningManager.findTravelBufferBetweenSiblingEvents(event, $scope.dayPlan);
            if (existingTravelBuffer) {
                _.remove($scope.dayPlan.travelBuffers, existingTravelBuffer);
            }

            if (shouldShowRouteSelection()) {
                $scope.routeForSelection = _.first($scope.routesForSelectionQueue);
                $scope.routeSelectionActive = true;
                $scope.$apply();
            } else {
                saveDayPlan($scope.dayPlan);
            }

        };

        $scope.onRouteSelected = (travelBuffer) => {

            $scope.routeSelectionActive = false;
            $scope.dayPlan.travelBuffers.push(travelBuffer);

            updateRouteSelectionQueue();

            if (shouldShowRouteSelection()) {
                $scope.routeForSelection = _.first($scope.routesForSelectionQueue);
                $scope.routeSelectionActive = true;
            } else {
                saveDayPlan($scope.dayPlan);
            }

        };

        $scope.onRouteSelectionCanceled = () => {

            $scope.routeSelectionActive = false;
            updateRouteSelectionQueue();

            if (shouldShowRouteSelection()) {

                $scope.routeForSelection = _.first($scope.routesForSelectionQueue);
                $scope.routeSelectionActive = true;

            } else {
                saveDayPlan($scope.dayPlan);
            }

        };

        $scope.filterFutureDates = (dates) => {
            dates.filter((date) => {
                return datesManager.isDateInPast(new Date(date.localDateValue()));
            }).forEach(function (date) {
                date.selectable = false;
            });
        };

        function updateRouteSelectionQueue() {
            $scope.routesForSelectionQueue = $scope.routesForSelectionQueue.slice(1);
        }

        function shouldShowRouteSelection() {

            return !!$scope.routesForSelectionQueue.length;

        }

        async function saveDayPlan(dayPlan) {

            $scope.dayPlanSaveState = 'Saving...';
            const savedDayPlan = await dayPlanManager.saveDayPlan(dayPlan);
            $scope.savedTimeStamp = new Date();

            setSavedAgoTime();
            executeStoredTasks();

            return savedDayPlan;

        }

        function setSavedAgoTime() {

            if ($scope.savedTimeStamp) {
                $scope.dayPlanSaveState = `Saved ${moment($scope.savedTimeStamp).fromNow()}`;
            }

        }

        function initSavedStateInterval() {

            $interval(() => {

                setSavedAgoTime();

            }, 30000);

        }

        function updateTaskStatus(task, newStatus) {
            if (task.isRegular) {
                return;
            }

            task.status = newStatus;
            return task;
        }

        async function executeStoredTasks() {

            let amountOfExecutedTasks = 0;

            for (let task of tasksToExecuteAfterSavingDayPlan) {
                if(_.isFunction(task)) {
                    await task();
                }
                amountOfExecutedTasks++;
            }

            tasksToExecuteAfterSavingDayPlan.splice(0, amountOfExecutedTasks, []);

        }

        $scope.$on('$destroy', function () {
            $interval.cancel($scope.savedStateInterval);
        });

        $rootScope.$on('newTaskCreated', $scope.updateSearchResults);

    }

})();
