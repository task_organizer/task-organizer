(function () {
    'use strict';

    angular.module('organizer.task-details').controller('taskDetailsCtrl', [
        '$scope',
        '$state',
        'priorityValueTextMapping',
        'toastr',
        'tasksManager',
        'datesManager',
        'dayPlanManager',
        'task',
        'myPlaces',
        'categories',
        taskDetailsCtrl
    ]);

    function taskDetailsCtrl($scope, $state, priorityValueTextMapping, toastr, tasksManager, datesManager, dayPlanManager, task, myPlaces, categories) {

        $scope.task = task;
        $scope.myPlaces = myPlaces;
        $scope.categories = categories;
        $scope.mapId = 'task-details';
        $scope.minDate = new Date();
        $scope.dayPlanDatePickerid = 'taskDetailsDatePicker' + (new Date()).valueOf();
        $scope.datePickerConfig = { dropdownSelector: `#${$scope.dayPlanDatePickerid}`, minView :'day' };
        $scope.filterFutureDates = (dates) => {
            dates.filter((date) => {
                return datesManager.isDateInPast(new Date(date.localDateValue()));
            }).forEach(function (date) {
                date.selectable = false;
            });
        };

        $scope.dateRangePickerOptions = {
            locale:               {
                applyLabel:  'Apply',
                fromLabel:   'From',
                format:      'DD.MMM.YYYY',
                toLabel:     'To',
                cancelLabel: 'Cancel'
            },
            ranges:               {
                'Today':      [moment(), moment().endOf('day')],
                'Tomorrow':   [moment(), moment().add(1, 'days').endOf('day')],
                'This week':  [moment(), moment().endOf('week').endOf('day')],
                'This month': [moment(), moment().endOf('month').endOf('day')]
            },
            weekStart:            0,
            autoclose:            false,
            showCustomRangeLabel: false,
            alwaysShowCalendars:  true
        };

        $scope.selectedDayPlanDate = new Date();

        $scope.onCategorySelected = function (selectedCategory) {
            const originalObject = _.get(selectedCategory, 'originalObject');
            $scope.task.category = _.get(originalObject, 'name');
        };

        $scope.$watch('task.priority', function (newValue) {

            $scope.taskImportanceLabel = priorityValueTextMapping[newValue];

        });

        $scope.filterCategories = function (searchString, categories) {

            let shouldAddCurrentSearchAsCategory = true;

            let result = _.filter(categories, function (category) {

                let categoryName = _.get(category, 'name');

                if (categoryName === searchString) {
                    shouldAddCurrentSearchAsCategory = false;
                }

                return categoryName.indexOf(searchString) > -1;

            });

            if (shouldAddCurrentSearchAsCategory) {

                result.push({name: searchString});

            }

            return result;

        };

        $scope.saveTask = async () => {
            await tasksManager.saveTask($scope.task);
            toastr.success('Task was saved');
            $scope.$emit('refreshTasks');
            $state.go('^');
        };

        $scope.closeTaskDetails = () => {
            $state.go('^');
        };

        $scope.addTaskAsTodoInDayPlan = async (date) => {

            const localDayOfYear = moment(date).dayOfYear();
            const localYear = moment(date).year();

            date = moment(date).utc().dayOfYear(localDayOfYear).year(localYear).hours(12).minutes(0);
            await dayPlanManager.addTaskAsTodoInDayPlan(date, $scope.task);
            toastr.success('Task was added to day plan todo list');

        };

    }

})();
