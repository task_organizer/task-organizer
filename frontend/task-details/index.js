(function() {
    'use strict';

    require('./../common/services');
    require('./../common/directives');

    angular.module('organizer.task-details', ['organizer.common.services', 'organizer.common.directives']);

    require('./controller');


})();