(function() {
    'use strict';

    angular.module('organizer.auth.registration', ['organizer.common.values']);

    require('./controller');

})();