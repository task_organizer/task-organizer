(function () {
    'use strict';

    angular.module('organizer.auth.registration').controller('registrationCtrl', [
        '$scope',
        '$state',
        'authManager',
        registrationCtrl]);

    function registrationCtrl($scope, $state, authManager) {

        $scope.user = {};

        $scope.register = function(form) {

            if(form.$invalid || !$scope.user.agreedTerms) {
                return;
            }

            authManager.register($scope.user).then(() => {
                $state.go('dashboardToday');
            });

        }

    }

})();
