(function () {
    'use strict';

    var AdminLTEOptions = {
        //Enable sidebar expand on hover effect for sidebar mini
        //This option is forced to true if both the fixed layout and sidebar mini
        //are used together
        sidebarExpandOnHover: true,
        //BoxRefresh Plugin
        enableBoxRefresh: true,
        //Bootstrap.js tooltip
        enableBSToppltip: true
    };

    window._ = require('lodash');
    window.jQuery = window.$ = require('jquery');

    require('jquery-ui-browserify');
    require('bootstrap');
    require('what-input');
    require('./../node_modules/admin-lte/dist/js/app.js');
    require('./../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js');
    require('angular-sanitize');
    require('angular-route');
    require('angular-animate');
    require('angular-messages');
    require('angular-resource');
    require('angular-toastr');
    require('angular-password');
    require('satellizer');
    require('angucomplete-alt');

    require('angular-bootstrap-datetimepicker');
    require('angular-ui-bootstrap');

    require('bootstrap-daterangepicker');
    require('angular-daterangepicker');

    require('./../node_modules/nouislider/distribute/jquery.nouislider.min.js');
    require('./../node_modules/angularnouislider/src/nouislider.js');
    require('datejs');
    require('angular-animate');
    require('angular-advanced-searchbox');
    require('angular-local-storage');
    require('angular-jwt');
    require('@uirouter/angularjs');
    require('ng-table');
    require('angular-bootstrap-confirm');

    window.moment = require('moment');
    moment.updateLocale('en', {
        week: {
            dow: 1 // Monday is the first day of the week
        }
    });
    require('moment-duration-format');

    require('fullcalendar');
    require('angular-ui-calendar');

})();