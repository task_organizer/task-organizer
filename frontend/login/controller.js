(function () {
    'use strict';

    angular.module('organizer.auth.login').controller('loginCtrl', [
        '$scope',
        'authManager',
        '$state',
        loginCtrl]);

    function loginCtrl($scope, authManager, $state) {

        $scope.login = function(form) {

            if(form.$invalid) {
                return;
            }

            authManager.login($scope.user).then(() => {
                $state.go('dashboardToday');
            });

        }

    }

})();
