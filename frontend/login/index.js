(function() {
    'use strict';

    angular.module('organizer.auth.login', ['organizer.common.values']);

    require('./controller');

})();