'use strict';

require('babel-polyfill');

var angular = require('angular');

require('./libraries');

require('./common/directives');
require('./common/services');
require('./common/models');
require('./common/formatters');

require('./dashboard');
require('./new-task');
require('./places');
require('./planning');
require('./login');
require('./registration');
require('./tasks');
require('./task-details');

var app = angular.module('organizer', [
    'ngRoute',
    'ngAnimate',
    'ngMessages',
    'ngSanitize',
    'satellizer',       // authorization lib
    'toastr',           // popup messages (toasts)
    'angucomplete-alt',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'daterangepicker',
    'nouislider',
    'ui.calendar',
    'angular-advanced-searchbox',
    'LocalStorageModule',
    'angular-jwt',
    'ui.router',
    'ngTable',
    'mwl.confirm',

    'organizer.common.directives',
    'organizer.common.services',
    'organizer.common.formatters',

    'organizer.dashboard',
    'organizer.tasks',
    'organizer.places',
    'organizer.planning',
    'organizer.tasks',
    'organizer.task-details',
    'organizer.auth.login',
    'organizer.auth.registration'
]);

app.run([
    '$rootScope', function ($rootScope) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            if (current.title) {
                $rootScope.title = current.title;
            } else {
                $rootScope.title = '404';
            }
        });
    }
]);

app.run([
    '$rootScope', '$location', '$auth', 'toastr', function ($rootScope, $location, $auth, toastr) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {

            var requireLogin = next.requireLogin || false;

            if (requireLogin && !$auth.isAuthenticated()) {
                event.preventDefault();

                toastr.error('You need to be authenticated to see this page');
                $rootScope.$evalAsync(function () {
                    $location.path(current || '/login').replace();
                });
            }

        });
    }
]);

app.run([
    'Restangular', function (restangular) {

        restangular.setBaseUrl('/api');

        restangular.configuration.getIdFromElem = function (data) {
            return _.get(data, '_id', '');
        };

        restangular.setRestangularFields({
            id: '_id'
        });

        restangular.addResponseInterceptor(function (data, operation, what, url, response, deferred) {

            var extractedData;

            if (operation === 'getList') {

                extractedData = data;
                extractedData._totalCount = Number(response.headers('totalCount'));

            } else {
                extractedData = data;
            }

            return extractedData;
        });

    }
]);

app.config([
    'localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('smartPlanning');
    }
]);

app.run([
    '$rootScope', function ($rootScope) {
        $rootScope.$on('$viewContentLoaded', function () {
            jQuery.AdminLTE.layout.activate();
        });
    }
]);

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app.run(['ngTableDefaults', function (ngTableDefaults) {

    ngTableDefaults.settings.interceptors = ngTableDefaults.settings.interceptors || [];
    ngTableDefaults.settings.interceptors.push({
        name: 'setTotalInterceptor',
        response: totalCountInterceptor
    });

    function totalCountInterceptor(data, params) {
        if (!data || !data.hasOwnProperty('_totalCount')) {
            return data;
        } else {
            params.total(data._totalCount);
            return data;
        }
    }

}]);

//Routes
require('./routes');

require('./authentication');
