import {extendDayPlanTransitLocationsArray} from '../models/restangularModelExtensions';

(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('dayPlanManager', ['$http', 'lodash', 'Restangular', dayPlanManagerService]);

    function dayPlanManagerService($http, _, Restangular) {

        Restangular.extendModel('dayplans', extendDayPlanTransitLocationsArray);
        let DayPlan = Restangular.all('dayplans');

        let service = {

            getDayPlan,
            createDayPlanEventFromTask,
            saveDayPlan,
            updateTodoInDayPlan,
            updateEventInDayPlan,
            addTaskAsTodoInDayPlan

        };

        return service;

        function getDayPlan(date) {

            date = moment(date);
            const query = {
                $and: [
                    {date: {$gte: date.startOf('day').toISOString()}},
                    {date: {$lte: date.endOf('day').toISOString()}}
                ]
            };

            return DayPlan.getList(query).then((dayPlans) => {

                const localDayOfYear = date.dayOfYear();
                const localYear = date.year();

                return _.first(dayPlans) || {
                        date:          date.utc().dayOfYear(localDayOfYear).year(localYear).hours(12).minutes(0),
                        events:        [],
                        todos:         [],
                        travelBuffers: []
                    };

            });

        }

        function saveDayPlan(dayPlan) {

            if (dayPlan._id) {

                return dayPlan.save();

            } else {

                return DayPlan.post(dayPlan);

            }

        }

        function createDayPlanEventFromTask(task, startDate, endDate) {

            if (!task) {
                throw new Error('task should be defined');
            }

            return {
                _id:      task._id + startDate + endDate,
                title:    task.title,
                start:    moment(startDate),
                end:      moment(endDate),
                location: _.get(task, 'location', {}),
                type:     'task',
                task:     {
                    ...task
                }
            }

        }

        async function updateTodoInDayPlan(dayPlan, task) {

            const response = await $http.put(`/api/updateTodoInDayPlan/${dayPlan._id}`, task);
            return response.data;

        }

        async function updateEventInDayPlan(dayPlan, task) {

            const response = await $http.put(`/api/updateEventInDayPlan/${dayPlan._id}`, task);
            return response.data;

        }

        async function addTaskAsTodoInDayPlan(date, task) {

            const response = await $http.post(`/api/addTaskAsTodoInDayPlan`, {
                date,
                task
            });
            return response.data;

        }

    }

})();