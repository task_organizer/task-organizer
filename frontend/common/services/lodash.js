(function () {
    'use strict';

    var _ = require('lodash');

    angular.module('organizer.common.services')
        .service('lodash', [lodashService]);

    function lodashService() {

        return _;

    }

})();
