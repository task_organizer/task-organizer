(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('authManager', ['$http', '$state', 'localStorageService', authManager]);

    function authManager($http, $state, localStorageService) {

        let service = {

            login,
            register,
            logout,
            getToken,
            getUserName,
            getLastLogin

        };

        return service;

        function login({email, password}) {

            const url = '/api/login';

            return $http.post(url,
                {
                    email,
                    password
                })
                .then(function (response) {

                    const user = response.data;
                    const token = user.token;

                    saveToken(token);
                    saveUserData(user);

                });

        }

        function register({fullname, email, password}) {

            const url = '/api/register';

            return $http.post(url,
                {
                    fullname,
                    email,
                    password
                })
                .then(function (response) {

                    const user = response.data;
                    const token = user.token;

                    saveToken(token);
                    saveUserData(user);

                });

        }

        function getUserName() {
            const {fullname} = localStorageService.get('user');
            return fullname;
        }

        function getLastLogin() {
            const {lastLogin} = localStorageService.get('user');
            return lastLogin;
        }

        function logout() {
            localStorageService.remove('jwtToken');
        }

        function saveToken(token) {
            localStorageService.set('jwtToken', token);
        }

        function saveUserData(user) {
            const userData = { ...user };
            delete userData.token;
            localStorageService.set('user', userData);
        }

        function getToken() {
            return localStorageService.get('jwtToken');
        }

    }

})();