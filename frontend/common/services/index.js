(function() {
    'use strict';

    require('angular-resource');
    require('restangular');

    require('./../values');

    angular.module('organizer.common.services', ['organizer.common.services', 'ngResource', 'restangular']);

    require('./geocoder.js');
    require('./placesResource.js');
    require('./placesService.js');
    require('./tasksManager.js');
    require('./lodash.js');
    require('./categoriesManager');
    require('./planningManager');
    require('./tasksFilterBuilder');
    require('./authManager');
    require('./travelManager');
    require('./dayPlanManager');
    require('./datesManager');

})();