(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('placesResource', ['$resource', 'placesUrl', myLocationsResourceFactory]);

    function myLocationsResourceFactory($resource, placesUrl) {

        return $resource(
            placesUrl + ':id',
            {
                id: '@_id'
            },
            {
                'update': { method:'PUT' }
            }
        );

    }

})();