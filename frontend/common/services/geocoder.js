(function () {
    'use strict';

    angular.module('organizer.common.services').factory('geocoding', myLocationsService);

    function myLocationsService() {

        var service = {
            geocode: geocode,
            reverseGeocode : reverseGeocode
        };

        return service;

        function geocode(address, callback) {

            var geocoder = new google.maps.Geocoder();
            var query = {
                'address': address
            };

            geocoder.geocode(query, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {
                        callback(results[0].geometry.location);
                    } else {
                        callback({});
                    }

                } else {
                    callback({});
                }

            });

        }

        function reverseGeocode(latitude, longitude, callback) {

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(latitude, longitude);
            var query = {
                'latLng': latlng
            };

            geocoder.geocode(query, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    if (results[0]) {
                        callback(results[0].formatted_address);
                    } else {
                        callback('Location not found');
                    }

                } else {
                    callback('Geocoder failed due to: ' + status);
                }

            });

        }
    }

})();