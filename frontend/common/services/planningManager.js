(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('planningManager', ['lodash', 'travelManager', 'placesService', 'travelBufferOptionDefaults', planningManagerService]);

    function planningManagerService(_, travelManager, placesService, travelBufferOptionDefaults) {

        let service = {

            updateTravelBuffersForShiftedEvent,
            getTravelBufferFromTo,
            getRoutesForSelection,
            createTravelBuffer,
            findTravelBufferBetweenSiblingEvents,
            getTravelBuffersForUnscheduledEvent,
            getSiblingEventsWithLocations,
            shouldHaveTravelBuffer,
            getRescheduledTravelBufferAfterReorderingEvents,
            isEventOrderChangedAfterRescheduling

        };

        return service;

        function getEarlierEventWithLocation(currentEvent, events) {

            const sortedEvents = _.sortBy(events, (event) => new Date(event.end));
            const indexOfCurrentEvent = _.indexOf(sortedEvents, currentEvent);

            if (indexOfCurrentEvent === 0) {
                return null;
            }

            const eventsBeforeCurrent = _.reverse(_.slice(sortedEvents, 0, indexOfCurrentEvent));

            const eventBeforeCurrentWithLocation = _.find(eventsBeforeCurrent, (previousEvent) =>
                previousEvent.location
            );

            return eventBeforeCurrentWithLocation;

        }

        function getNextEventWithLocation(currentEvent, events) {

            const sortedEvents = _.sortBy(events, (event) => new Date(event.end));
            const indexOfCurrentEvent = _.indexOf(sortedEvents, currentEvent);

            const eventsAfterCurrent = _.slice(sortedEvents, indexOfCurrentEvent + 1);

            const eventAfterCurrentWithLocation = _.find(eventsAfterCurrent, (nextEvent) =>
                nextEvent.location
            );

            return eventAfterCurrentWithLocation;

        }

        function shouldHaveTravelBuffer(fromEvent, toEvent) {

            const enoughData = fromEvent && toEvent && fromEvent.location && toEvent.location;
            return enoughData && travelManager.areLocationsDifferent(fromEvent.location, toEvent.location);

        }

        function getSiblingEventsWithLocations(event, events) {

            return {
                previousEvent: getEarlierEventWithLocation(event, events),
                nextEvent: getNextEventWithLocation(event, events)
            }

        }

        function findTravelBufferBetweenSiblingEvents(event, dayPlan) {

            const siblingEvents = getSiblingEventsWithLocations(event, dayPlan.events);

            if (siblingEvents.previousEvent && siblingEvents.nextEvent) {

                return findTravelBufferForEvents(siblingEvents.previousEvent, siblingEvents.nextEvent, dayPlan.travelBuffers);

            } else if(siblingEvents.nextEvent) {

                return findTravelBufferForEvents({_id: undefined}, siblingEvents.nextEvent, dayPlan.travelBuffers);

            }

        }

        function findTravelBufferForEvents(fromEvent, toEvent, travelBuffers) {

            return _.find(travelBuffers, (travelBuffer) => {
                return _.get(fromEvent, '_id') === travelBuffer.fromEventId && _.get(toEvent, '_id') === travelBuffer.toEventId;
            });

        }

        async function getTravelBufferFromTo(fromEvent, toEvent, {scheduleBeforeNextEvent, departureBuffer, arrivalBuffer}) {

            let originLocation = _.get(fromEvent, 'location');

            if (!originLocation) {
                originLocation = await placesService.getHome();
            }

            const originLatLng = {
                lat: originLocation.latitude,
                lng: originLocation.longitude,
            };
            const destinationLatLng = {
                lat: toEvent.location.latitude,
                lng: toEvent.location.longitude,
            };

            departureBuffer = isNaN(departureBuffer) ? travelBufferOptionDefaults.departureBuffer : departureBuffer;
            arrivalBuffer = isNaN(arrivalBuffer) ? travelBufferOptionDefaults.arrivalBuffer : arrivalBuffer;

            const departureTimeWithBuffer = calculateDepartureTimeWithBuffer(fromEvent.end, departureBuffer);
            let arrivalTimeWithBuffer = null;

            if (scheduleBeforeNextEvent) {
                arrivalTimeWithBuffer = calculateArrivalTimeWithBuffer(toEvent.start, arrivalBuffer);
            }

            const groupedRoutes = await travelManager.getGroupedRoutesForAllTransportTypes(
                originLatLng,
                destinationLatLng,
                departureTimeWithBuffer,
                arrivalTimeWithBuffer
            );

            const fromEventTitle = _.get(fromEvent, 'title');
            const toEventTitle = _.get(toEvent, 'title');
            const title = `"${fromEventTitle}" -> "${toEventTitle}"`;

            // add duration

            return {
                groupedRoutes,
                fromEvent,
                toEvent,
                fromEventId: _.get(fromEvent, '_id'),
                toEventId: _.get(toEvent, '_id'),
                departureTime: departureTimeWithBuffer,
                arrivalTime: arrivalTimeWithBuffer,
                departureBuffer,
                arrivalBuffer,
                title
            }

        }

        function createTravelBuffer(travelBufferInfo, travelMode, scheduleBeforeNextEvent) {

            const groupedRoutesForTravelMode = _.find(travelBufferInfo.groupedRoutes, {travelMode});
            const duration = _.get(groupedRoutesForTravelMode, 'duration');

            let travelBufferStart, travelBufferEnd;
            let totalDuration = moment.duration(duration, 'seconds')
                .add(travelBufferInfo.departureBuffer, 'seconds')
                .add(travelBufferInfo.arrivalBuffer, 'seconds');
            const durationText = moment.duration(totalDuration, 'seconds').format('h [hours] m [mins]');

            if (scheduleBeforeNextEvent) {

                travelBufferEnd = moment(travelBufferInfo.arrivalTime)
                    .add(travelBufferInfo.arrivalBuffer, 'seconds');
                travelBufferStart = moment(travelBufferEnd).subtract(totalDuration, 'seconds');

            } else {

                travelBufferStart = travelBufferInfo.departureTime
                    .subtract(travelBufferInfo.departureBuffer, 'seconds');
                travelBufferEnd = moment(travelBufferStart).add(totalDuration, 'seconds');

            }

            travelManager.deleteRoutesForUnusedTravelModes(travelMode, travelBufferInfo.groupedRoutes);

            const newTravelBuffer = {
                ...travelBufferInfo,
                duration: totalDuration,
                directions: travelBufferInfo.groupedRoutes,
                groupedRoutes: undefined,
                travelMode,
                title: `(${durationText}) ${travelBufferInfo.title}`,
                start: travelBufferStart,
                end: travelBufferEnd
            };

            return newTravelBuffer;

        }

        async function getRoutesForSelection(event, dayPlan) {

            let routesForSelection = [];
            const siblingEvents = getSiblingEventsWithLocations(event, dayPlan.events);

            if (siblingEvents.previousEvent && shouldHaveTravelBuffer(siblingEvents.previousEvent, event)) {

                routesForSelection.push({fromEvent: siblingEvents.previousEvent, toEvent: event});

            } else if (!siblingEvents.previousEvent && await travelManager.isLocationDifferentFromHome(event.location)) {

                const homeEvent = {
                    title: 'Home',
                    location: await placesService.getHome(),
                    isHomeEvent: true
                };

                routesForSelection.push({fromEvent: homeEvent, toEvent: event});

            }

            if (siblingEvents.nextEvent && shouldHaveTravelBuffer(event, siblingEvents.nextEvent)) {

                routesForSelection.push({fromEvent: event, toEvent: siblingEvents.nextEvent});

            }

            return routesForSelection;

        }

        async function recalculateTravelBuffer(travelBuffer, {departureTime, arrivalTime}) {

            if (!departureTime && !arrivalTime) {
                throw new Error('planningManager.recalculateTravelBuffer() travel buffer recalculation departure or arrival time should be defined');
            }

            const travelMode = travelBuffer.travelMode;
            const directions = travelBuffer.directions;

            const routeInfo = _.get(travelBuffer, 'directions.' + travelMode);
            const routeRequest = _.get(routeInfo, 'route.request');

            const originLatLng = _.get(routeRequest, 'origin.location');
            const destinationLatLng = _.get(routeRequest, 'destination.location');

            let actualDepartureTime, actualArrivalTime;

            if (departureTime) {

                const departureBuffer = moment.duration(travelBuffer.departureBuffer, 'seconds');
                actualDepartureTime = moment(departureTime).add(departureBuffer);

            }

            if (arrivalTime) {

                const arrivalBuffer = moment.duration(travelBuffer.arrivalBuffer, 'seconds');
                actualArrivalTime = moment(arrivalTime).subtract(arrivalBuffer);

            }

            const recalculatedRoute = await
                travelManager.calculateRouteForTravelMode(travelMode,
                    originLatLng, destinationLatLng,
                    actualDepartureTime, actualArrivalTime
                );

            const oldTravelBufferDirections = _.get(travelBuffer, 'directions');

            const newTravelBufferDirections = {
                ...oldTravelBufferDirections
            };

            newTravelBufferDirections[travelMode] = recalculatedRoute;

            const totalDuration = moment.duration(travelBuffer.departureBuffer || 0, 'seconds')
                .add(travelBuffer.arrivalBuffer || 0, 'seconds')
                .add(recalculatedRoute.duration);

            const newStart = arrivalTime ? moment(arrivalTime).subtract(totalDuration) : departureTime;
            const newEnd = departureTime ? moment(departureTime).add(totalDuration) : arrivalTime;
            const newTitle = _.replace(travelBuffer.title, /\(.*?\)/, `(${getDurationText(totalDuration)})`);

            return {
                ...travelBuffer,
                directions: newTravelBufferDirections,
                start: newStart,
                end: newEnd,
                title: newTitle
            };

        }

        async function updateTravelBuffersForShiftedEvent(event, travelBuffers, {eventOldStartTime, eventOldEndTime}) {

            if (!eventOldStartTime && !eventOldEndTime) {
                throw new Error('planningCtrl.recalculateTravelBuffersForEvent() - travel buffers recalculation not possible without eventOldStartTime or eventOldEndTime');
            }

            if (eventOldStartTime) {

                const isStartTimeDifferent = Math.abs(new Date(event.start) - new Date(eventOldStartTime)) > 10 * 1000;

                if (isStartTimeDifferent) {
                    await recalculatePrecedingTravelBufferForEvent(event, travelBuffers, eventOldStartTime);
                }

            }

            if (eventOldEndTime) {

                const isEndTimeDifferent = Math.abs(new Date(event.end) - new Date(eventOldEndTime)) > 10 * 1000;

                if (isEndTimeDifferent) {
                    await recalculateFollowingTravelBufferForEvent(event, travelBuffers, eventOldEndTime);
                }

            }

        }

        async function recalculatePrecedingTravelBufferForEvent(event, travelBuffers, oldStartTime) {

            const precedingTravelBuffer = getTravelBufferRightBeforeTheEvent(travelBuffers, oldStartTime);

            if (precedingTravelBuffer) {

                const recalculationParameters = {
                    arrivalTime: event.start
                };

                const newTravelBuffer = await recalculateTravelBuffer(precedingTravelBuffer, recalculationParameters);
                updateTravelBufferInDayPlan(travelBuffers, precedingTravelBuffer, newTravelBuffer);

            }

        }

        async function recalculateFollowingTravelBufferForEvent(event, travelBuffers, oldEndTime) {

            const followingTravelBuffer = getTravelBufferRightAfterTheEvent(travelBuffers, oldEndTime);

            if (followingTravelBuffer) {

                const recalculationParameters = {
                    departureTime: event.end
                };

                const newTravelBuffer = await recalculateTravelBuffer(followingTravelBuffer, recalculationParameters);
                updateTravelBufferInDayPlan(travelBuffers, followingTravelBuffer, newTravelBuffer);

            }

        }

        function getTravelBufferRightAfterTheEvent(travelBuffers, eventEndTime) {

            return _.find(travelBuffers, (travelBuffer) => {

                return Math.abs((new Date(travelBuffer.start) - new Date(eventEndTime))) < (60 * 1000);

            });

        }

        function getTravelBufferRightBeforeTheEvent(travelBuffers, eventStartTime) {

            return _.find(travelBuffers, (travelBuffer) => {

                return Math.abs((new Date(travelBuffer.end) - new Date(eventStartTime))) < (60 * 1000);

            });

        }

        function updateTravelBufferInDayPlan(travelBuffers, travelBufferToUpdate, newTravelBuffer) {

            const oldTravelBufferIndex = _.indexOf(travelBuffers, travelBufferToUpdate);
            travelBuffers.splice(oldTravelBufferIndex, 1, newTravelBuffer);

        }

        function getDurationText(momentDuration) {

            return momentDuration.format('h [hours] m [mins]');

        }

        function calculateDepartureTimeWithBuffer(departureTime, departureBuffer) {

            return moment(departureTime).add(departureBuffer, 'seconds');

        }

        function calculateArrivalTimeWithBuffer(arrivalTime, arrivalBuffer) {

            return moment(arrivalTime).subtract(arrivalBuffer, 'seconds');

        }

        function getTravelBuffersForUnscheduledEvent(event, travelBuffers) {

            const eventId = _.get(event, '_id');
            return _.filter(travelBuffers, (travelBuffer) => travelBuffer.fromEventId === eventId || travelBuffer.toEventId === eventId);

        }

        function getSiblingEventsFromStartEndTime(events, startTime, endTime) {

            events = _.map(events, (event) => {
                return {
                    ...event,
                    start: moment(event.start),
                    end: moment(event.end)
                }
            });
            const sortedEvents = _.sortBy(events, (event) => new Date(event.end));

            const precedingEvent = _.last(_.filter(sortedEvents, (event) => event.start < startTime));
            const followingEvent = _.first(_.filter(sortedEvents, (event) => event.end > endTime));

            return {
                precedingEvent,
                followingEvent
            };

        }

        function getTravelBuffersForEvent(travelBuffers, event) {

            return _.filter(travelBuffers, (travelBuffer) => {

                return travelBuffer.fromEventId === event._id || travelBuffer.toEventId === event._id;

            });

        }

        async function getRescheduledTravelBufferAfterReorderingEvents(dayPlan, event, oldStartTime, oldEndTime) {

            const events = dayPlan.events;
            const travelBuffers = dayPlan.travelBuffers;
            let routesForSelection = [];

            const travelBuffersToDelete = getTravelBuffersForEvent(travelBuffers, event);
            _.remove(travelBuffers, (tb) => _.some(travelBuffersToDelete, tb));

            routesForSelection = await getRoutesForSelection(event, dayPlan);

            const existingTravelBuffer = findTravelBufferBetweenSiblingEvents(event, dayPlan);
            if (existingTravelBuffer) {
                _.remove(travelBuffers, existingTravelBuffer);
            }

            const {precedingEvent: oldPrecedingEvent, followingEvent: oldFollowingEvent} = getSiblingEventsFromStartEndTime(events, oldStartTime, oldEndTime);
            if (shouldHaveTravelBuffer(oldPrecedingEvent, oldFollowingEvent)) {
                routesForSelection.push({fromEvent: oldPrecedingEvent, toEvent: oldFollowingEvent});
            }

            return routesForSelection;

        }

        function isEventOrderChangedAfterRescheduling(events, rescheduledEvent, oldStartTime, oldEndTime) {

            events = _.map(events, (event) => {
                return {
                    ...event,
                    start: moment(event.start),
                    end: moment(event.end)
                }
            });
            const sortedEvents = _.sortBy(events, (event) => event.end);

            const oldPrecedingEvent = _.last(_.filter(sortedEvents, (event) => event.start < oldStartTime));
            const oldFollowingEvent = _.first(_.filter(sortedEvents, (event) => event.end > oldEndTime));

            const currentPrecedingEvent = _.last(_.filter(sortedEvents, (event) => event.start < rescheduledEvent.start));
            const currentFollowingEvent = _.first(_.filter(sortedEvents, (event) => event.end > rescheduledEvent.end));

            return oldPrecedingEvent !== currentPrecedingEvent && oldFollowingEvent !== currentFollowingEvent;

        }

    }

})();