(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('categoriesManager', ['lodash', 'Restangular', tasksManagerService]);

    function tasksManagerService(_, Restangular) {

        let categories = Restangular.all('categories');

        let service = {

            getTasksForCategories,
            getAllCategories,
            getNMostPopularCategories

        };

        return service;

        function getTasksForCategories(categories) {

            return categories.get(categories);

        }

        function getAllCategories() {

            return categories.getList();

        }

        function getNMostPopularCategories(n) {

            return getAllCategories().then((result) => _.take(result, n));

        }

    }

})();