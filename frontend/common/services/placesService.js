(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('placesService', ['$q',  'geocoding', 'placesResource', placesService]);

    function placesService($q, geocoding, placesResource) {

        var data;

        var service = {

            getMyPlaces,
            addNewPlace,
            savePlace,
            deletePlace,
            getHome,
            getAddressCoordinates

        };

        return service;

        function getMyPlaces(refresh) {

            if (data && !refresh) {

                var deferrer = $q.defer();
                deferrer.resolve(data);
                return deferrer.promise;

            } else {

                return placesResource.query().$promise.then(function (response) {

                    data = response;
                    return data;

                });

            }

        }

        async function getHome() {

            const myPlaces = await getMyPlaces();
            return _.find(myPlaces, (place) => place.name.toLowerCase() === 'home');

        }

        async function addNewPlace(place) {

            const resource = new placesResource(place);
            const newPlace = await resource.$save();

            data.splice(data.indexOf(place), 1, newPlace);
            return data;

        }

        function savePlace(place) {

            return placesResource.update({id: place._id}, place).$promise;

        }

        function deletePlace(place) {

            return placesResource.remove({id: place._id}).$promise.then(function () {

                data.splice(data.indexOf(place), 1);

            });

        }

        function getAddressCoordinates(address) {

            const deferred = $q.defer();
            const callback = function (coordinates) {

                if (coordinates && coordinates.lat && coordinates.lng) {

                    const location = {
                        latitude: coordinates.lat(),
                        longitude: coordinates.lng()
                    };
                    deferred.resolve(location);

                } else {

                    deferred.reject();

                }

            };

            geocoding.geocode(address, callback);

            return deferred.promise;


        }

    }

})();