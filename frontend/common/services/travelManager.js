(function () {
    'use strict';

    const geodist = require('geodist');

    angular.module('organizer.common.services')
        .service('travelManager', ['$q', 'placesService', 'travelModes', travelManagerService]);

    function travelManagerService($q, placesService, travelModeNames) {

        let service = {
            getGroupedRoutesForAllTransportTypes,
            isLocationDifferentFromHome,
            areLocationsDifferent,
            deleteRoutesForUnusedTravelModes,
            calculateRouteForTravelMode
        };

        const travelModes = {
            driving: 'DRIVING',
            walking: 'WALKING',
            bicycling: 'BICYCLING',
            transit: 'TRANSIT'
        };

        let directionsService;

        if (window.google) {

            directionsService = new window.google.maps.DirectionsService;

        } else {
            directionsService = {
                route: function () {
                    return Promise((resolve) => {
                        resolve(null)
                    });
                }
            }
        }

        return service;

        function getGroupedRoutesForAllTransportTypes(originLatLng, destinationLatLng, departureTime, arrivalTime) {

            return $q.all([

                calculateRouteForTravelMode('driving', originLatLng, destinationLatLng, departureTime, arrivalTime),
                calculateRouteForTravelMode('walking', originLatLng, destinationLatLng, departureTime, arrivalTime),
                calculateRouteForTravelMode('bicycling', originLatLng, destinationLatLng, departureTime, arrivalTime),
                calculateRouteForTravelMode('transit', originLatLng, destinationLatLng, departureTime, arrivalTime)

            ]).then(([driving, walking, bicycling, transit]) => {

                return {
                    ...{driving},
                    ...{walking},
                    ...{bicycling},
                    ...{transit},
                };

            });

        }

        async function calculateRouteForTravelMode(travelMode, originLatLng, destinationLatLng, departureTime, arrivalTime) {

            // if(isDrivingModeWithArrivalTime(travelMode, arrivalTime)) {
            //
            //     // get departureTime
            //
            // } else {

            const timeParameters = getTimeRequestObject(departureTime, arrivalTime);
            const request = formatRequestForTravelMode(travelMode, originLatLng, destinationLatLng, timeParameters);

            try {

                const directions = await performDirectionsRequest(request);

                return {
                    travelMode: travelMode,
                    duration: getRouteDuration(directions),
                    route: directions
                }

            } catch (err) {

                return null;

            }

            // }


        }

        function isDrivingModeWithArrivalTime(travelMode, arrivalTime) {

            return travelMode === 'driving' && arrivalTime;

        }

        function formatRequestForTravelMode(travelMode, originalLatLng, destinationLatLng, options) {

            const requestFormatter = getRequestFormatterForTravelMode(travelMode);

            return requestFormatter(originalLatLng, destinationLatLng, options);

        }

        function getRequestFormatterForTravelMode(travelMode) {

            const requestFormatters = {
                driving: formatDrivingRouteCalculationRequest,
                bicycling: formatBicyclingRouteCalculationRequest,
                transit: formatPublicTransportRouteCalculationRequest,
                walking: formatWalkingRouteCalculationRequest
            };

            return requestFormatters[travelMode];

        }

        function formatBicyclingRouteCalculationRequest(originLatLng, destinationLatLng) {

            const staticRequestParameters = {
                origin: originLatLng,
                destination: destinationLatLng
            };

            const requestParameters = {
                ...staticRequestParameters,
                travelMode: travelModes.bicycling
            };

            return requestParameters;

        }

        function formatPublicTransportRouteCalculationRequest(originLatLng, destinationLatLng, options) {

            const staticRequestParameters = {
                origin: originLatLng,
                destination: destinationLatLng
            };

            const requestParameters = {
                ...staticRequestParameters,
                travelMode: travelModes.transit,
                transitOptions: {...options}
            };

            return requestParameters;

        }

        function formatWalkingRouteCalculationRequest(originLatLng, destinationLatLng) {

            const staticRequestParameters = {
                origin: originLatLng,
                destination: destinationLatLng
            };

            const requestParameters = {
                ...staticRequestParameters,
                travelMode: travelModes.walking,
            };

            return requestParameters;

        }

        function formatDrivingRouteCalculationRequest(originLatLng, destinationLatLng, options) {

            const staticRequestParameters = {
                origin: originLatLng,
                destination: destinationLatLng
            };

            const requestParameters = {

                ...staticRequestParameters,

                travelMode: travelModes.driving,
                drivingOptions: {
                    trafficModel: 'pessimistic',
                    departureTime: options.departureTime || options.arrivalTime
                }

            };

            return requestParameters;

        }

        function performDirectionsRequest(requestParams) {

            const deferred = $q.defer();

            directionsService.route(requestParams, function (response, status) {
                if (status == 'OK') {
                    deferred.resolve(response);
                } else {
                    deferred.reject();
                    console.warn('Directions request failed due to ' + status);
                }
            });

            return deferred.promise;

        }

        function getRouteDuration(directions) {

            const durationInTraffic = _.get(directions, 'routes[0].legs[0].duration_in_traffic');
            const normalDuration = _.get(directions, 'routes[0].legs[0].duration');
            const durationSeconds = _.get(durationInTraffic || normalDuration, 'value');

            return moment.duration(durationSeconds, 'seconds');

        }

        function deleteRoutesForUnusedTravelModes(usedTravelMode, groupedTravelBuffers) {

            _.forEach(travelModeNames, (travelModeName) => {

                const travelBufferForTravelMode = groupedTravelBuffers[travelModeName];

                if (travelBufferForTravelMode && travelModeName !== usedTravelMode) {

                    const route = _.get(travelBufferForTravelMode, 'route');
                    delete route.routes;

                }

            });

        }

        function getTimeRequestObject(departureTime, arrivalTime) {

            const timeParameters = {};

            if (arrivalTime) {

                timeParameters.arrivalTime = new Date(arrivalTime);

            }

            if (departureTime) {

                timeParameters.departureTime = new Date(departureTime);

            }

            return timeParameters;

        }

        async function isLocationDifferentFromHome(location) {
            const home = await placesService.getHome();
            return areLocationsDifferent(home, location);
        }

        function areLocationsDifferent(location1, location2) {

            const origin = {
                lat: _.get(location1, 'latitude'),
                lon: _.get(location1, 'longitude')
            };

            const destination = {
                lat: _.get(location2, 'latitude'),
                lon: _.get(location2, 'longitude')
            };

            const options = {
                unit: 'meters'
            };

            return geodist(origin, destination, options) > 100;

        }

    }

})();