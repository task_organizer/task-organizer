import {extendTask} from '../models/restangularModelExtensions';

(function () {
    'use strict';

    angular.module('organizer.common.services')
        .service('tasksManager', ['Restangular', 'tasksFilterBuilder', tasksManagerService]);

    function tasksManagerService(Restangular, tasksFilterBuilder) {

        Restangular.extendModel('tasks', extendTask);
        let tasks = Restangular.all('tasks');

        let service = {

            createQuickTask,
            createTask,
            saveTask,
            deleteTask,
            getTasks,
            getTaskById,
            getUnspecifiedTasks: getTasks.bind(this, {isUnspecified: true}),
            getTasksForDay,
            getNotscheduledTasksWithAssignedDay,
            getTasksForWeek

        };

        return service;

        function createQuickTask(quickTask) {

            return tasks.post(quickTask);

        }

        function createTask(task) {

            return tasks.post(task);

        }

        function saveTask(task) {

            return task.put();

        }

        function deleteTask(task) {

            return task.remove();

        }

        function getTasks(filter) {

            return tasks.getList(filter);

        }

        function getTasksForDay(day) {

            const filter = tasksFilterBuilder.getDatesFilterForDay(day);

            return tasks.getList(filter);

        }

        function getTaskById(id) {

            return Restangular.one('tasks', id).get();

        }

        function getNotscheduledTasksWithAssignedDay(day) {

            const filter = tasksFilterBuilder.getDatesFilterForDay(day);
            const notScheduledFilter = {
                status: 'notscheduled'
            };

            return tasks.getList({...filter, ...notScheduledFilter});
        }

        function getTasksForWeek(day) {

            const filter = tasksFilterBuilder.getTasksForWeekFilter(day);
            return tasks.getList(filter);

        }

    }

})();