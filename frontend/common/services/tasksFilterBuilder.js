(function () {
    'use strict';

    angular.module('organizer.common.services').factory('tasksFilterBuilder', tasksFilterBuilder);

    function tasksFilterBuilder() {

        const service = {
            getDatesFilterForDay,
            getTasksForWeekFilter,
            transformSearchParamsToRegexpQuery,
            buildFilterFromSearchString,
            getCriticalNotScheduledTasksFilter
        };

        return service;

        function buildFilterFromSearchString(searchString) {

            const query = {
                $or: [
                    transformSearchParamsToRegexpQuery({title: searchString}),
                    transformSearchParamsToRegexpQuery({category: searchString}),
                    transformSearchParamsToRegexpQuery({description: searchString})
                ]
            };

            return query;

        }

        function getCriticalNotScheduledTasksFilter() {

            return {
                status: 'notscheduled',
                priority: {
                    $gt: 3
                }
            }

        }

        function getDatesFilterForDay(day) {

            const dayBegin = moment(day).startOf('day');
            const dayEnd = moment(day).endOf('day');

            const filter = {
                'time.onDate': {
                    $gte: dayBegin,
                    $lte: dayEnd
                }
            };

            return filter;

        }

        function getTasksForWeekFilter(day) {

            const startDate = moment(day).startOf('day');
            const endDate = moment(day).endOf('week');

            const filter = {

                $or: [
                    {
                        'time.onDate': {
                            $gte: startDate,
                            $lte: endDate
                        }
                    },

                    {
                        $and: [
                            {
                                'time.startDate': {$lte: endDate}
                            },
                            {
                                'time.endDate': {$gte: startDate},
                            }]
                    }
                ]

            };

            return filter;

        }

        function transformSearchParamsToRegexpQuery(searchParams, stringParamsProcessor) {

            const notProcessedPropertyNames = ['status', 'isRegular'];

            // check the filterKey - for each query key construct regexp (1 | 2 | 3)
            return _.reduce(_.keys(searchParams), (constructedQuery, filterKey) => {

                let searchParamsForCurrentKey = searchParams[filterKey];
                if (searchParamsForCurrentKey) {

                    let resultQuery = {...constructedQuery};

                    if (notProcessedPropertyNames.indexOf(filterKey) > -1) {

                        resultQuery[filterKey] = searchParamsForCurrentKey;

                    } else if (isDateSearchParameter(filterKey)) {

                        resultQuery = {
                            ...resultQuery,
                            ...transformDateSearchParams(filterKey, searchParamsForCurrentKey)
                        };

                    } else if (_.isArray(searchParamsForCurrentKey) && !isArrayOfPrimitives(searchParamsForCurrentKey)) {

                        resultQuery[filterKey] = _.map(searchParamsForCurrentKey, (searchParam) => transformSearchParamsToRegexpQuery(searchParam, stringParamsProcessor));

                    } else if (_.isObject(searchParamsForCurrentKey) && !_.isArray(searchParamsForCurrentKey)) {

                        resultQuery[filterKey] = [...transformSearchParamsToRegexpQuery(searchParamsForCurrentKey, stringParamsProcessor)];

                    } else {

                        let possibleValuesRegexp = searchParamsForCurrentKey;

                        if (_.isString(searchParamsForCurrentKey) && _.isFunction(stringParamsProcessor)) {

                            possibleValuesRegexp = stringParamsProcessor(searchParamsForCurrentKey);

                        }

                        if (_.isArray(possibleValuesRegexp)) {

                            possibleValuesRegexp = _.map(possibleValuesRegexp, (searchValues) => {
                                return searchValues.toString()
                            }).join('|');

                        }

                        resultQuery[filterKey] = {$regex: possibleValuesRegexp, $options: 'i'};

                    }

                    return resultQuery;

                } else {

                    return constructedQuery;

                }

            }, {});

        }

        function isDateSearchParameter(paramName) {
            return paramName.indexOf('time.') === 0;
        }

        function isArrayOfPrimitives(array) {

            return _.every(array, (elem) => !_.isObject(elem));

        }

        function transformDateSearchParams(dateSearchParam, value) {

            let query = {};

            if (dateSearchParam === 'time.endDate') {

                // intersections of task window and search is handled
                query['time.startDate'] = {
                    $lte: value.toISOString()
                };

            } else if (dateSearchParam === 'time.startDate') {

                // intersections of task window and search is handled
                query['time.endDate'] = {
                    $gte: value.toISOString()
                };

            }

            return query;

        }

    }

})();