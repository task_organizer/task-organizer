(function () {
    'use strict';

    var _ = require('lodash');

    angular.module('organizer.common.services')
        .service('datesManager', [datesManager]);

    function datesManager() {

        return {
            isDateInPast
        };

        function isDateInPast(date) {

            date = new Date(date);
            const todaySinceMidnight = new Date();
            todaySinceMidnight.setHours(0,0,0,0);
            return date < todaySinceMidnight;

        }

    }

})();
