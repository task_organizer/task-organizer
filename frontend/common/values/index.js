(function () {
    'use strict';

    var valuesModule = angular.module('organizer.common.values', []);

    // DateTimePicker options
    valuesModule.value('dateTimePickerOptions', {
        format: 'dd.mm.yyyy hh:ii',
        disableDblClickSelection: true,
        pickTime: true
    }).value('datePickerOptions', {
        format: 'dd.mm.yyyy',
        disableDblClickSelection: true,
        pickTime: false
    });

    // Importance DropDown options
    valuesModule.value('priorityValueTextMapping', ['Very Low', 'Low', 'Medium', 'High', 'Very High']);

    valuesModule.value('placesUrl', '/api/places/');

    valuesModule.value('taskStatuses', ['notscheduled', 'scheduled', 'inprogress', 'done']);

    valuesModule.value('planningDayCalendarConfig', {
        height: 450,
        editable: true,
        droppable: true,
        firstDay: 1,
        defaultView: 'agendaDay',
        header: false,
        timeFormat: 'H:mm',
        slotLabelFormat: 'H:mm',
        allDaySlot: false,
        slotDuration: moment.duration(15, 'minutes')
        // scrollTime:  moment().subtract(1, 'hours').startOf('hour')
    });

    valuesModule.value('defaultPlanEventDuration', '00:30'); // 30 minutes

    valuesModule.value('travelModes', ['driving', 'bicycling', 'transit', 'walking']);

    valuesModule.value('tasksStatusFilter', [
        {
            id: 'notscheduled',
            title: 'Not Scheduled'
        },
        {
            id: 'scheduled',
            title: 'Scheduled'
        },
        {
            id: 'done',
            title: 'Done'
        },
        {
            id: 'regular',
            title: 'Regular'
        }
    ]);

    valuesModule.value('travelModesIcons', {
        'driving': 'fa-car',
        'transit': 'fa-bus',
        'bicycling': 'fa-bicycle',
        'walking': 'fa-male'
    });

    valuesModule.value('travelBufferOptionDefaults', {
        departureBuffer: 300,
        arrivalBuffer: 300
    });

})();