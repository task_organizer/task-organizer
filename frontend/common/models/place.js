(function () {
    'use strict';

    angular.module('organizer.common.models')
        .factory('place', [placeFactory]);

    function placeFactory() {

        class place {

            constructor({name, latitude, longitude, address, information}) {

                this.name = name || '';
                this.information = information || '';

                this.latitude = latitude || 0;
                this.longitude = longitude || 0;

                this.address = address || '';

            }

        }

        place.create = function (data) {

            return new place(data || {});

        };

        return place;

    }

})();
