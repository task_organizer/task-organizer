(function () {
    'use strict';

    angular.module('organizer.common.models')
        .factory('quickTask', [quickTaskFactory]);

    function quickTaskFactory() {

        class quickTask {

            constructor({name, description, relevantStartDate, relevantEndDate}) {
                if(arguments)

                this.name = name || '';
                this.description = description || '';
                this.relevantStartDate = relevantStartDate || new Date();
                this.relevantEndDate = relevantEndDate || null;
                this.isUnspecified = true;
            }


        }

        quickTask.create = function (data) {

            return new quickTask(data || {});

        };

        return quickTask;

    }

})();
