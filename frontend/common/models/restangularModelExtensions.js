module.exports = {
    extendTask: (task) => {
        task.setLocation = function (location) {
            this.location = location;
        };

        return task;
    },
    extendDayPlanTransitLocationsArray: (dayplan) => {

        dayplan.travelBuffers = _.map(dayplan.travelBuffers, (travelBuffer) => {

            if (travelBuffer.travelMode === 'transit') {

                const route = _.get(travelBuffer, 'directions.transit.route');

                if (route && route.routes.length) {

                    const googleMapsRoute = route.routes[0];

                    if (googleMapsRoute.overview_path && googleMapsRoute.overview_path.length) {
                        googleMapsRoute.overview_path = _.map(googleMapsRoute.overview_path, (pathPoint) => (new google.maps.LatLng(pathPoint)));
                    }

                    if (googleMapsRoute.legs && googleMapsRoute.legs.length) {

                        const leg = googleMapsRoute.legs[0];
                        convertLegToLanLng(leg);

                    }

                }

            }

            return travelBuffer;

        });

        return dayplan;

    }
};

function convertLegToLanLng(leg) {

    leg.end_location = new google.maps.LatLng(leg.end_location);
    leg.end_point = new google.maps.LatLng(leg.end_point);
    leg.start_location = new google.maps.LatLng(leg.start_location);
    leg.start_point = new google.maps.LatLng(leg.start_point);

    if (leg.lat_lngs) {
        leg.lat_lngs = _.map(leg.lat_lngs, (latLng) => (new google.maps.LatLng(latLng)));
    }

    if (leg.path) {
        leg.path = _.map(leg.path, (pathPoint) => (new google.maps.LatLng(pathPoint)));
    }

    if (leg.steps) {
        leg.steps = _.map(leg.steps, (step) => convertLegToLanLng(step));
    }

    return leg;

}