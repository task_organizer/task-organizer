(function() {
    'use strict';

    angular.module('organizer.common.models', []);

    require('./quickTask');
    require('./task');
    require('./place')

})();