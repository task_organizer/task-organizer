(function () {
    'use strict';

    angular.module('organizer.common.models')
        .factory('task', [taskFactory]);

    function taskFactory() {

        class task {

            constructor({title, description, location, category, priority, status, time, isRegular}) {

                this.title = title || '';
                this.description = description || '';

                this.location = location || {};
                this.category = category || '';
                this.priority = priority || 2;
                this.status = status || 'notscheduled';
                this.isRegular = isRegular || false;

                this.time = {
                    startDate: (time && time.startDate) || moment(),
                    endDate:   (time && time.endDate) || moment().add(30, 'days'),
                    duration:  (time && time.duration) || null
                }

            }

            updateStatus(newStatus) {
                this.status = newStatus;
            };

            setLocation(location) {
                this.location = location;
            }

        }

        task.create = function (data) {

            return new task(data || {});

        };

        return task;

    }

})();
