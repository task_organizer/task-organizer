(function () {
    'use strict';

    angular.module('organizer.common.directives.sortableDraggableList').directive('sortableDraggableList', function () {
        return {
            restrict: 'A',
            scope:    {
                draggableOptions:    '=',
                onDropCallback:      '&',
                ngModel:             '=',
                updateOrderCallback: '&'
            },
            link(scope, element) {

                $(element).disableSelection().sortable({

                    stop: function (event, ui) {

                        const taskId = $(event.toElement).data('taskId');

                        if (taskId && ui.item && ui.item.index && _.isFunction(ui.item.index)) {

                            const newIndex = ui.item.index();
                            const taskObject = _.find(scope.ngModel, {_id: taskId});
                            const oldIndex = _.indexOf(scope.ngModel, taskObject);

                            scope.ngModel.splice(oldIndex, 1);
                            scope.ngModel.splice(newIndex, 0, taskObject);
                            scope.$apply();

                            if(_.isFunction(scope.updateOrderCallback)) {
                                scope.updateOrderCallback();
                            }

                        }

                    }

                });

            }
        };
    });

})();