(function () {
    'use strict';

    angular.module('organizer.common.directives.sortableDraggableList', []);

    require('./directive');

})();