(function () {
    'use strict';

    angular.module('organizer.common.directives.draggableElement').directive('draggableElement', function () {
        return {
            restrict: 'A',
            scope:    {
                draggableOptions: '=',
                onDropCallback:   '&',
                ngModel:          '='
            },
            link(scope, element) {

                $(element).draggable({
                    ...scope.draggableOptions,
                    appendTo:    'body',
                    containment: 'window',
                    scroll:      false,
                    helper:      'clone',
                    stop:        () => {

                        if (_.isFunction(scope.onDropCallback)) {
                            scope.onDropCallback(scope.ngModel);
                        }

                    }
                });

            }
        };
    });

})();