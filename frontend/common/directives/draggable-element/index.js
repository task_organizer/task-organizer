(function () {
    'use strict';

    angular.module('organizer.common.directives.draggableElement', []);

    require('./directive');

})();