(function () {
    'use strict';

    angular.module('organizer.common.directives.dayEventsTimeline').directive('dayEventsTimeline', function () {
        return {
            restrict: 'E',
            templateUrl: '/partials/common/directives/day-events-timeline/view',
            scope: {
                taskTitleProperty: '@',
                showCheckedState: '@',

                events: '=',
                travelBuffers: '=',
                checkedPredicate: '=',

                updateStateForTaskCallback: '&'
            },
            controller: [
                '$scope', 'travelModesIcons', ($scope, travelModesIcons) => {

                    $scope.travelModesIcons = travelModesIcons;
                    $scope.$watchCollection('events', (updatedEvents) => {
                        if ($scope.showCheckedState) {
                            _.each(updatedEvents, (event) => {
                                _.extend(event, {isDone: event.task.status === 'done'})
                            });
                        }
                    });

                    $scope.timelineItems = _.sortBy(_.concat($scope.events, $scope.travelBuffers), ['end']);

                    $scope.isTravelBuffer = function(timelineItem) {
                        return !timelineItem.task && timelineItem.travelMode;
                    };

                    $scope.updateEventTaskStatusIsDone = (event, isDone) => {

                        event.task.status = isDone ? 'done' : 'scheduled';
                        $scope.updateStateForTaskCallback({event: event});

                    }

                }
            ]
        };
    });

})();