(function () {
    'use strict';

    angular.module('organizer.common.directives.dayEventsTimeline', []);

    require('./directive');

})();