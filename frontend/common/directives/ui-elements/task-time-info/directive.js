(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('taskTimeInfo', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                task: '<'
            },
            controller: [
                '$scope', function ($scope) {

                    const task = $scope.task;
                    $scope.startTime = moment(task.time.onDate).format('H:mm');
                    const duration = moment.duration(task.time.duration);
                    $scope.endTime = moment(task.time.onDate).add(duration).format('H:mm');
                    $scope.durationHours = duration.asHours();

                }
            ],
            template: '<span>{{startTime}} - {{endTime}} (~{{durationHours}} hours)</span>'
        };
    });

})();