(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('taskAddressSearchInput', function () {
        return {
            restrict: 'E',
            scope: {
                task: '='
            },
            controller: [
                '$scope', 'place', 'placesService', function ($scope, Place, placesService) {

                    $scope.address = $scope.task.location.address;

                    $scope.updateAddress = async function () {

                        const {latitude, longitude} = await placesService.getAddressCoordinates($scope.address);

                        const location = {
                            name: '',
                            latitude: latitude,
                            longitude: longitude,
                            information: '',
                            address: $scope.address
                        };
                        $scope.task.setLocation(new Place(location));
                        $scope.$apply();

                    };

                }
            ],
            templateUrl: '/partials/common/directives/ui-elements/task-address-search-input/view',
        };
    });

})();