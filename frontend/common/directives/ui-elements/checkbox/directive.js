(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('checkbox', function () {
        return {
            restrict:    'E',
            replace:     true,
            transclude:  true,
            scope:       {
                ngModel:  '=',
                required: '@',
                onChange: '&'
            },
            controller:  [
                '$scope', function ($scope) {
                    $scope.toggleCheck = function () {
                        $scope.ngModel = !$scope.ngModel;
                        $scope.onChange({newValue: $scope.ngModel});
                    }
                }
            ],
            templateUrl: '/partials/common/directives/ui-elements/checkbox/view',
        };
    });

})();