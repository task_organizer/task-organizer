(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('priorityIcon', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                priority: '@'
            },
            controller: [
                '$scope', function ($scope) {

                    $scope.priorityIcons = {
                        '0': ['fa-long-arrow-down', 'fa-long-arrow-down'],
                        '1': ['fa-long-arrow-down'],
                        '2': ['fa-long-arrow-right'],
                        '3': ['fa-long-arrow-up'],
                        '4': ['fa-long-arrow-up', 'fa-long-arrow-up']
                    };

                }
            ],
            templateUrl: '/partials/common/directives/ui-elements/priority-icon/view',
        };
    });

})();