(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('taskLocationPicker', function () {
        return {
            restrict: 'E',
            scope: {
                myPlaces: '<',
                task: '=',
                selectedLocation: '=',
                useGoogleDirections: '<',
                inputClass: '@'
            },
            controller: [
                '$scope', 'place', 'geocoding', function ($scope, Place, geocodingService) {

                    $scope.filterMyPlaces = function (searchString, places) {

                        const regex = new RegExp(searchString, 'i');
                        let result = _.filter(places, function (place) {

                            return regex.test(place.name) || regex.test(place.address);

                        });

                        return result;

                    };

                    $scope.onTaskPlaceSelected = function (selection) {

                        if (selection) {

                            const originalObject = _.get(selection, 'originalObject');

                            if (originalObject.latitude) {

                                $scope.task.setLocation(new Place(originalObject));

                            } else {

                                const callback = function (coordinates) {

                                    if(coordinates.lat && coordinates.lon) {

                                        const taskPlaceData = {
                                            name: originalObject.name,
                                            latitude: coordinates.lat(),
                                            longitude: coordinates.lng(),
                                            information: originalObject.description,
                                            address: originalObject.address
                                        };

                                        $scope.task.setLocation(new Place(taskPlaceData));

                                    }

                                };

                                geocodingService.geocode(originalObject.address, callback);

                            }

                        }

                    };

                    $scope.placeRequestFormatter = (searchString) => {

                        const deferred = $q.defer();
                        let foundMyPlaces = $scope.filterMyPlaces(searchString, $scope.myPlaces);

                        foundMyPlaces = _.map(foundMyPlaces, (myPlace) => _.extend(myPlace, {isMyPlace: true}));

                        if (searchString.length > 0) {

                            const autocompleteService = new google.maps.places.AutocompleteService();

                            autocompleteService.getPlacePredictions({
                                input: searchString,
                                offset: searchString.length
                            }, (foundAddresses) => {

                                const googleResults = _.chain(foundAddresses)
                                    .map((searchResult) => {

                                        const formattedValues = _.get(searchResult, 'structured_formatting');

                                        if (formattedValues) {

                                            searchResult.name = _.get(formattedValues, 'main_text');
                                            searchResult.address = _.get(formattedValues, 'secondary_text');

                                        }

                                        return searchResult;

                                    })
                                    .filter((value) => value)
                                    .value();

                                const result = _.concat(foundMyPlaces, googleResults);

                                deferred.resolve(result);

                            });

                        } else {

                            deferred.resolve($scope.myPlaces);

                        }

                        return deferred.promise;

                    };
                }
            ],
            templateUrl: '/partials/common/directives/ui-elements/task-location-picker/view',
        };
    });

})();