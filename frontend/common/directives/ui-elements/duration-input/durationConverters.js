const _ = require('lodash');
const moment = require('moment');

const unitsIdentifiers = {
    hours:   'h',
    minutes: 'm',
    seconds: 's',
    weeks:   'w'
};

function formatDuration(momentDuration) {

    if(!moment.isDuration(momentDuration)) {
        return '';
    }

    const hours = momentDuration.hours();
    const minutes = momentDuration.minutes();
    const weeks = momentDuration.weeks();
    const seconds = momentDuration.seconds();

    let hoursText = '';
    let minutesText = '';
    let weeksText = '';
    let secondsText = '';

    if(hours) hoursText = hours + unitsIdentifiers.hours;
    if(minutes) minutesText = minutes + unitsIdentifiers.minutes;
    if(weeks) weeksText = weeks + unitsIdentifiers.weeks;
    if(seconds) secondsText = seconds + unitsIdentifiers.seconds;

    return _.filter([weeksText, hoursText, minutesText, secondsText]).join(' ');

}

function createDurationFromText(durationText) {

    durationText = durationText.toLowerCase();
    const units = _.values(unitsIdentifiers);

    const {totalDuration} = _.reduce(durationText, ({totalDuration, collectedValue}, currentCharacter) => {

        if(units.indexOf(currentCharacter) > -1) {

            const currentUnitDuration = moment.duration(Number.parseFloat(collectedValue), currentCharacter);

            return {
                totalDuration: totalDuration.add(currentUnitDuration),
                collectedValue: ''
            };

        } else {

            return {
                totalDuration,
                collectedValue: collectedValue.concat(currentCharacter)
            };

        }

    }, {totalDuration: moment.duration(), collectedValue: ''});

    return totalDuration;

}

module.exports = {
    formatDuration,
    createDurationFromText
};