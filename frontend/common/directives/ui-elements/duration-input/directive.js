import {formatDuration, createDurationFromText} from './durationConverters';

(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('durationInput', function () {
        return {
            restrict:    'E',
            scope:       {
                ngModel:  '=',
                required: '@'
            },
            link:        ($scope) => {

                if (!moment) {
                    throw new Error('duration-input - moment is undefined');
                }

                if ($scope.ngModel && !moment.duration($scope.ngModel).isValid()) {
                    throw new Error('duration-input - ngModel is not a correct moment duration');
                }

            },
            controller:  [
                '$scope', function ($scope) {

                    $scope.textDuration = formatDuration(moment.duration($scope.ngModel));

                    $scope.$watch('textDuration', (newValue) => {

                        if (newValue) {

                            $scope.ngModel = createDurationFromText(newValue);

                        }

                    });

                }
            ],
            templateUrl: '/partials/common/directives/ui-elements/duration-input/view',
        };
    });

})();