(function() {
    'use strict';

    angular.module('organizer.common.directives.ui-elements', []);

    require('./checkbox');
    require('./duration-input');
    require('./priority-icon');
    require('./task-status-icon');
    require('./task-time-info');
    require('./task-deadline-info');
    require('./task-location-picker');
    require('./task-address-search-input');

})();