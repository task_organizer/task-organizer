(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('taskDeadlineInfo', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                task: '<'
            },
            controller: [
                '$scope', function ($scope) {

                    const task = $scope.task;
                    const deadline = _.get(task, 'time.endDate');
                    $scope.deadline = moment(deadline).format('ddd DD.MM.Y');

                }
            ],
            template: '<span>{{deadline}}</span>'
        };
    });

})();