(function () {
    'use strict';

    angular.module('organizer.common.directives.ui-elements').directive('taskStatusIcon', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                task: '<'
            },
            controller: [
                '$scope', '$timeout', function ($scope, $timeout) {

                    $scope.statusIcons = {
                        'notscheduled': {
                            icon: 'fa-calendar-times-o',
                            title: 'Not yet added to the day plan'
                        },
                        'scheduled': {
                            icon: 'fa-calendar-check-o',
                            title: 'Already scheduled to some day plan',
                            colorClass: 'text-green'
                        },
                        'done': {
                            icon: 'fa-check',
                            title: 'Task was marked as done',
                            colorClass: 'text-green'
                        },
                        'regularTask': {
                            icon: 'fa-repeat',
                            title: 'Regular task - execution is repeating over certain time spans',
                            colorClass: 'text-aqua'
                        }
                    };

                    const animationClasses = 'animated bounceInDown';
                    $scope.animationClasses = '';

                    $scope.$watch(
                        'task',
                        (updatedTask, oldTask) => {

                            if (updatedTask) {

                                const oldStatus = _.get(oldTask, 'status');
                                const newStatus = _.get(updatedTask, 'status');

                                if (oldStatus && oldStatus !== newStatus) {
                                    $scope.animationClasses = animationClasses;
                                    $timeout(() => {
                                        $scope.animationClasses = '';
                                    }, 1000);
                                }

                                $scope.taskStatus = _.get(updatedTask, 'isRegular') === true ? 'regularTask' : newStatus;
                            }
                        },
                        true);

                }
            ],
            templateUrl: '/partials/common/directives/ui-elements/task-status-icon/view',
        };
    });

})();