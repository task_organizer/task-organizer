(function () {
    'use strict';

    angular.module('organizer.common.directives.droppableContainer').directive('droppableContainer', function () {
        return {
            restrict: 'A',
            scope: {
                droppableOptions: '=',
                onDropCallback: '&'
            },
            link(scope, element) {

                $(element).droppable({
                    ...scope.droppableOptions,
                    drop: function (event, ui) {

                        const droppedElement = ui.draggable;
                        const taskId = $(droppedElement).data('taskId');

                        if(_.isFunction(scope.onDropCallback)) {
                            scope.onDropCallback({taskId});
                        }

                    }
                });

            }
        };
    });

})();