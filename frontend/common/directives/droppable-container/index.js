(function () {
    'use strict';

    angular.module('organizer.common.directives.droppableContainer', []);

    require('./directive');

})();