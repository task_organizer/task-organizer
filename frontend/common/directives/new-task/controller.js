(function () {
    'use strict';

    angular.module('organizer.common.directives.newtask')
        .controller('newTaskDirectiveCtrl', [
            '$scope',
            '$rootScope',
            '$q',
            'dateTimePickerOptions',
            'priorityValueTextMapping',
            'toastr',
            'placesService',
            'categoriesManager',
            'geocoding',
            'tasksManager',
            'task',
            'place',
            'NgMap',
            newTaskCtrl
        ]);

    function newTaskCtrl($scope, $rootScope, $q, datePickerOptions, priorityValueTextMapping, toastr,
                         placesService, categoriesManager,
                         geocodingService, tasksManager, Task, Place, NgMap) {

        $scope.categories = [];
        $scope.datePickerOptions = datePickerOptions;
        $scope.priorityValueTextMapping = priorityValueTextMapping;

        $scope.taskHasNoExactTime = true;
        $scope.taskImportanceLabel = 'Medium';

        $scope.minDate = new Date();
        $scope.mapId = 'newTaskMap';

        $scope.task = new Task({});

        $scope.dateRangePickerOptions = {
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                format: 'DD.MMM.YYYY',
                toLabel: 'To',
                cancelLabel: 'Cancel'
            },
            ranges: {
                'Today': [moment(), moment().endOf('day')],
                'Tomorrow': [moment().add(1, 'days').startOf('day'), moment().add(1, 'days').endOf('day')],
                'This week': [moment(), moment().endOf('week').endOf('day')],
                'This month': [moment(), moment().endOf('month').endOf('day')]
            },
            weekStart: 0,
            showCustomRangeLabel: false,
            alwaysShowCalendars: true,
            linkedCalendars: false
        };


        placesService.getMyPlaces().then((places) => $scope.myPlaces = places);

        categoriesManager.getAllCategories().then((categories) => {
            $scope.categories = _.map(categories, (category) => {
                return {name: category}
            });
        });

        $scope.tryCreateNewTask = function (form, newTask) {

            if(form.$invalid) {
                return;
            }

            newTask = preprocessTaskBeforeSaving(newTask);

            tasksManager.createTask(newTask)
                .then(() => {
                    $scope.$emit('newTaskDialog.taskCreated');
                    $rootScope.$emit('newTaskCreated');
                })
                .then(() => {
                    toastr.success('Task was created')
                })
                .catch(() => {
                    toastr.error('Task was not created')
                });

        };

        $scope.filterMyPlaces = function (searchString, places) {

            const regex = new RegExp(searchString, 'i');
            let result = _.filter(places, function (place) {

                return regex.test(place.name) || regex.test(place.address);

            });

            return result;

        };

        $scope.placeRequestFormatter = function (searchString) {

            const deferred = $q.defer();
            let foundMyPlaces = $scope.filterMyPlaces(searchString, $scope.myPlaces);

            foundMyPlaces = _.map(foundMyPlaces, (myPlace) => _.extend(myPlace, {isMyPlace: true}));

            if (searchString.length > 0) {

                const autocompleteService = new google.maps.places.AutocompleteService();

                autocompleteService.getPlacePredictions({
                    input: searchString,
                    offset: searchString.length
                }, (foundAddresses) => {

                    const googleResults = _.chain(foundAddresses)
                        .map((searchResult) => {

                            const formattedValues = _.get(searchResult, 'structured_formatting');

                            if (formattedValues) {

                                searchResult.name = _.get(formattedValues, 'main_text');
                                searchResult.address = _.get(formattedValues, 'secondary_text');

                            }

                            return searchResult;

                        })
                        .filter((value) => value)
                        .value();

                    const result = _.concat(foundMyPlaces, googleResults);

                    deferred.resolve(result);

                });

            } else {

                deferred.resolve($scope.myPlaces);

            }

            return deferred.promise;

        };

        $scope.taskPlaceSelected = function (selection) {

            if (selection) {

                const originalObject = _.get(selection, 'originalObject');

                if (originalObject.latitude) {

                    $scope.task.setLocation(new Place(originalObject));

                } else {

                    const callback = function (coordinates) {

                        const taskPlaceData = {
                            name: originalObject.name,
                            latitude: coordinates.lat(),
                            longitude: coordinates.lng(),
                            information: originalObject.description,
                            address: originalObject.address
                        };

                        $scope.task.setLocation(new Place(taskPlaceData));

                    };

                    geocodingService.geocode(originalObject.address, callback);

                }
            }

        };

        $scope.categorySelected = function (selectedCategory) {
            const originalObject = _.get(selectedCategory, 'originalObject');
            $scope.task.category = _.get(originalObject, 'name');
        };

        $scope.filterCategories = function (searchString, categories) {

            let shouldAddCurrentSearchAsCategory = true;

            let result = _.filter(categories, function (category) {

                let categoryName = _.get(category, 'name');

                if (categoryName === searchString) {
                    shouldAddCurrentSearchAsCategory = false;
                }

                return categoryName.indexOf(searchString) > -1;

            });

            if (shouldAddCurrentSearchAsCategory) {

                result.push({name: searchString});

            }

            return result;

        };

        $scope.myPlaceLocationIsSelected = function (location) {

            const originalObject = _.get(location, 'originalObject');
            $scope.task.setLocation(new Place(originalObject));

        };

        $scope.$watch('foundPlace', function () {

            var selectedPlace = _.get($scope, 'foundPlace');

            if (selectedPlace) {

                _.set($scope, 'newTask.location', selectedPlace.originalObject);

            }

        });

        $scope.$watch('task.priority', function (newValue) {

            $scope.taskImportanceLabel = priorityValueTextMapping[newValue];

        });

        $scope.updateAddress = async function () {

            $scope.newTaskPlaceSearch = '';

            const {latitude, longitude} = await placesService.getAddressCoordinates($scope.task.location.address);
            $scope.task.location.name = $scope.task.location.address;
            $scope.task.location.latitude = latitude;
            $scope.task.location.longitude = longitude;
            $scope.$apply();

        };

        $scope.reactivateMap = async function () {

            const map = await NgMap.getMap($scope.mapId);
            google.maps.event.trigger(map, 'resize');

        };

        function preprocessTaskBeforeSaving(task, taskHasNoExactTime) {

            if(taskHasNoExactTime) {
                delete task.time.onDate;
            } else {
                delete task.time.startDate;
                delete task.time.endDate;
            }

            return task;

        }

    }

})();
