(function () {
    'use strict';

    angular.module('organizer.common.directives.newtask', ['organizer.common.services', 'ngMapAutocomplete', 'ngMap']);

    require('./controller');
    require('./directive');

})();