(function () {
    'use strict';

    angular.module('organizer.common.directives.newtask').directive('newTask', function () {
        return {
            restrict: 'E',
            scope: {
                task: '='
            },
            controller: 'newTaskDirectiveCtrl',
            templateUrl: '/partials/common/directives/new-task/view',
            link: (scope, element) => {

                const taskTitleInput = $(element).find('#newTaskName').get(0);
                const $taskTitleInput = $(taskTitleInput);

                if ($taskTitleInput) {
                    const checkInterval = setInterval(() => {

                        if ($taskTitleInput.is(':visible')) {

                            clearInterval(checkInterval);
                            setTimeout(() => {
                                $taskTitleInput.trigger('focus');
                            }, 800);

                        }

                    }, 100);
                }
            }
        };
    });

})();