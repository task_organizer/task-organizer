(function () {
    'use strict';

    require('./navbar');
    require('./todo-list');
    require('./locationPicker');
    require('./new-task');
    require('./tooltip');
    require('./main-sidebar');
    require('./modal');
    require('./draggable-element');
    require('./ui-elements');
    require('./task-search-input');
    require('./sortable-draggable-list');
    require('./droppable-container');
    require('./todo-list');
    require('./day-agenda');
    require('./route-selection');
    require('./day-plan-map');
    require('./date-picker');
    require('./task-search-results');
    require('./date-time-picker');
    require('./day-events-timeline');

    angular.module('organizer.common.directives', [
        'organizer.common.directives.navbar',
        'organizer.common.directives.todoList',
        'organizer.common.directives.locationPicker',
        'organizer.common.directives.newtask',
        'organizer.common.directives.tooltip',
        'organizer.common.directives.mainSidebar',
        'organizer.common.directives.modal',
        'organizer.common.directives.draggableElement',
        'organizer.common.directives.ui-elements',
        'organizer.common.directives.taskSearchInput',
        'organizer.common.directives.sortableDraggableList',
        'organizer.common.directives.droppableContainer',
        'organizer.common.directives.todoList',
        'organizer.common.directives.dayAgenda',
        'organizer.common.directives.routeSelection',
        'organizer.common.directives.dayPlanMap',
        'organizer.common.directives.datePicker',
        'organizer.common.directives.taskSearchResults',
        'organizer.common.directives.dateTimePicker',
        'organizer.common.directives.dayEventsTimeline'
    ]);

})();