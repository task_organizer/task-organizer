(function () {
    'use strict';

    angular.module('organizer.common.directives.dayAgenda').directive('dayAgenda', function () {
        return {
            restrict: 'E',
            templateUrl: '/partials/common/directives/day-agenda/view',
            scope: {
                calendarConfig: '=',

                events: '=',
                travelBuffers: '=',
                calendarName: '@',
                selectedDate: '=',
                tasksSource: '=',

                allowDropOut: '@',

                onDayEventsUpdate: '&',
                onTaskUpdate: '&',
                onNewEventAdded: '&',
                onEventDurationChanged: '&',
                onEventRescheduled: '&',
                onEventUnscheduled: '&'
            },
            controller: [
                '$scope',
                'uiCalendarConfig',
                'defaultPlanEventDuration',
                'dayPlanManager',
                ($scope, uiCalendarConfig, defaultPlanEventDuration, dayPlanManager) => {

                    $scope.dayCalendarConfig = {
                        height: 450,
                        eventStartEditable: false,
                        eventDurationEditable: false,
                        droppable: false,
                        firstDay: 1,
                        defaultView: 'agendaDay',
                        header: false,
                        timeFormat: 'H:mm',
                        timezone: 'local',
                        slotLabelFormat: 'H:mm',
                        allDaySlot: false,
                        defaultDate: $scope.selectedDate,
                        dragRevertDuration: 0,
                        ...$scope.calendarConfig
                    };

                    $scope.calendarName = $scope.calendarName || ('dayAgenda' + (new Date()).valueOf());

                    // allow move events
                    if ($scope.calendarConfig.eventStartEditable) {

                        $scope.dayCalendarConfig = {
                            ...$scope.dayCalendarConfig,
                            eventDrop: function (droppedEvent, delta, revertFunc, jsEvent) {

                                const rescheduledEvent = _.find($scope.events, (scheduledEvent) => scheduledEvent._id === droppedEvent._id);

                                if (rescheduledEvent) {

                                    const oldStartTime = moment(rescheduledEvent.start);
                                    const oldEndTime = moment(rescheduledEvent.end);

                                    rescheduledEvent.start = moment(droppedEvent.start);
                                    rescheduledEvent.end = moment(droppedEvent.end);

                                    refreshCalendar();
                                    $scope.onEventRescheduled({event: rescheduledEvent, oldStartTime, oldEndTime});

                                }

                            }
                        }

                    }

                    // allow resize events
                    if ($scope.calendarConfig.eventDurationEditable) {

                        $scope.dayCalendarConfig = {
                            ...$scope.dayCalendarConfig,
                            eventResize: function (resizedEvent, delta, revertFunc, jsEvent) {

                                const event = _.find($scope.events, {_id: resizedEvent._id});
                                if (event) {

                                    const oldEndTime = event.end;
                                    event.end = resizedEvent.end;
                                    refreshCalendar();

                                    const eventParameters = {
                                        event,
                                        oldEndTime
                                    };

                                    $scope.onEventDurationChanged(eventParameters);
                                }

                            }
                        }

                    }

                    // allow drop events outside of calendar to unschedule them
                    if ($scope.allowDropOut) {

                        $scope.dayCalendarConfig = {
                            ...$scope.dayCalendarConfig,
                            eventDragStop: function (droppedEvent, jsEvent) {

                                const calendarContainer = $('ui-calendar .fc-view-container');
                                const calendarTopLeft = calendarContainer.offset();
                                const calendarBottomRight = {
                                    bottom: calendarTopLeft.top + calendarContainer.outerHeight(),
                                    right: calendarTopLeft.left + calendarContainer.outerWidth()
                                };

                                const eventDroppedInsideOfCalendar = jsEvent.pageX > calendarTopLeft.left && jsEvent.pageX < calendarBottomRight.right &&
                                    jsEvent.pageY > calendarTopLeft.top && jsEvent.pageY < calendarBottomRight.bottom;

                                if (!eventDroppedInsideOfCalendar) {

                                    const eventToBeRemoved = _.find($scope.events, (scheduledEvent) => {

                                        if (!scheduledEvent._id || !droppedEvent._id || (scheduledEvent._id.length !== droppedEvent._id.length)) {

                                            return _.isEqual(_.get(scheduledEvent, 'task._id'), _.get(droppedEvent, 'task._id'))

                                        } else {

                                            return scheduledEvent._id === droppedEvent._id;

                                        }

                                    });

                                    $scope.onEventUnscheduled({event: eventToBeRemoved});
                                    setTaskNewStatus(_.get(droppedEvent, 'task._id'), 'notscheduled');
                                    refreshCalendar();

                                }

                            }
                        }

                    }

                    // allow drop external events
                    if ($scope.calendarConfig.droppable) {

                        $scope.dayCalendarConfig = {
                            ...$scope.dayCalendarConfig,
                            drop: function (momentDate, event, ui, resourceId) {

                                const taskId = $(this).data('taskId');
                                let task = getTaskById(taskId);

                                if (task) {
                                    task = _.isFunction(task.plain) ? task.plain() : task;
                                    const taskDuration = _.get(task, 'time.duration') || defaultPlanEventDuration;
                                    const taskEndDate = moment(momentDate).add(moment.duration(taskDuration));

                                    const dayPlanEvent = dayPlanManager.createDayPlanEventFromTask(task, momentDate, taskEndDate);

                                    $scope.events.push(dayPlanEvent);

                                    setTaskNewStatus(taskId, 'scheduled');
                                    refreshCalendar();

                                    if (_.isFunction($scope.onNewEventAdded)) {
                                        $scope.onNewEventAdded({event: dayPlanEvent});
                                    }

                                }

                            }
                        };

                    }

                    function getTaskById(taskId) {
                        return _.find($scope.tasksSource, {_id: taskId});
                    }

                    function setTaskNewStatus(taskId, status) {

                        const task = getTaskById(taskId);

                        if(!task.isRegular) {
                            task.status = status;
                        }

                        if (task) {
                            $scope.onTaskUpdate(task);
                        }

                    }

                    const refreshCalendar = () => {

                        uiCalendarConfig.calendars[$scope.calendarName].fullCalendar('removeEvents');

                        if ($scope.events && $scope.events.length && uiCalendarConfig.calendars[$scope.calendarName]) {

                            uiCalendarConfig.calendars[$scope.calendarName].fullCalendar('addEventSource', $scope.events);

                            const travelBuffersData = {
                                events: $scope.travelBuffers,
                                color: 'lightgrey',
                                textColor: 'grey',
                                startEditable: false,
                                durationEditable: false
                            };

                            uiCalendarConfig.calendars[$scope.calendarName].fullCalendar('addEventSource', travelBuffersData);

                        }

                    };

                    $scope.$watchCollection('travelBuffers', (travelBuffers) => {

                        if (travelBuffers && travelBuffers.length) {
                            refreshCalendar();
                        }

                    });

                    $scope.$watchCollection('events', (events) => {
                        if(events && events.length) {
                            refreshCalendar();
                        }
                    });

                    setTimeout(() => {
                        refreshCalendar();
                    }, 1);

                }
            ]
        };
    });

})();