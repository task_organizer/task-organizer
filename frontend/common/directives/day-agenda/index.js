(function () {
    'use strict';

    angular.module('organizer.common.directives.dayAgenda', []);

    require('./directive');

})();