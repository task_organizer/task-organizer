(function () {
    'use strict';

    require('angular-advanced-searchbox');

    angular.module('organizer.common.directives.taskSearchInput', ['angular-advanced-searchbox', 'organizer.common.services']);

    require('./directive');

})();