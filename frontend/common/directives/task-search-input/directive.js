(function () {
    'use strict';

    angular.module('organizer.common.directives.taskSearchInput').directive('taskSearchInput', function () {
        return {
            restrict: 'E',
            ngModel: true,
            scope: {
                ngModel: '='
            },
            templateUrl: '/partials/common/directives/task-search-input/view',
            controller: [
                '$scope', '$q', 'categoriesManager', 'placesService', function ($scope, $q, categoriesManager, placesService) {

                    $scope.query = {};

                    $q.all([categoriesManager.getNMostPopularCategories(10), placesService.getMyPlaces()]).then(([categories, myPlaces]) => {

                        const suggestedCategories = _.map(categories, (category) => ({
                            name: category
                        }));

                        const suggestedPlaces = _.map(myPlaces, (place) => ({
                            name: place.name
                        }));

                        $scope.query = angular.copy($scope.ngModel);

                        $scope.availableSearchParams = [
                            {
                                key: 'title',
                                name: 'Title',
                                placeholder: 'clean the kitchen',
                                allowMultiple: true
                            },
                            {
                                key: 'category',
                                name: 'Category',
                                placeholder: 'work...',
                                suggestedValues: suggestedCategories,
                                suggestedToString: function (category) {
                                    return (category && category.name) || '';
                                },
                                allowMultiple: true
                            },
                            {
                                key: 'location.name',
                                name: 'Place name',
                                placeholder: 'home...',
                                suggestedValues: suggestedPlaces,
                                suggestedToString: function (place) {
                                    return place && place.name;
                                },
                                allowMultiple: true
                            },
                            {
                                key: 'places.address',
                                name: 'Address',
                                placeholder: 'Munich...',
                                allowMultiple: true
                            },
                            {
                                key: 'time.startDate',
                                name: 'Relevant after',
                                placeholder: 'today...',
                                type: 'date',
                                suggestedToString: function (date) {
                                    return date.toDateString ? date.toDateString() : date.toString();
                                }
                            },
                            {
                                key: 'time.endDate',
                                name: 'Deadline',
                                placeholder: 'tomorrow...',
                                type: 'date',
                                suggestedToString: function (date) {
                                    return date.toDateString ? date.toDateString() : date.toString();
                                }
                            }
                        ];

                    });

                    $scope.$on('advanced-searchbox:modelUpdated', (event, newModel) => {

                        if (newModel.category && newModel.category.length) {

                            newModel = {
                                ...newModel,
                                category: _.map(_.filter(newModel.category), (category) => category.name)
                            };

                        }

                        if (_.get(newModel, 'location.name') && _.get(newModel, 'location.name').length) {

                            newModel = {
                                ...newModel,
                                'location.name': _.map(_.filter(newModel['location.name']), (place) => place.name)
                            };

                        }

                        $scope.ngModel = newModel;

                    });

                    $scope.$on('advanced-searchbox:removedAllSearchParam', () => {
                        $scope.ngModel = {};
                    });

                }
            ]
        };
    });

})();