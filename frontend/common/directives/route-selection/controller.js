(function () {
    'use strict';

    angular.module('organizer.common.directives.routeSelection')
        .controller('routeSelectionCtrl', [
            '$scope',
            'NgMap',
            'travelModes',
            'travelModesIcons',
            'travelBufferOptionDefaults',
            'planningManager',
            routeSelectionCtrl
        ]);

    function routeSelectionCtrl($scope, NgMap, travelModes, travelModesIcons, travelBufferOptionDefaults, planningManager) {

        let directionsDisplay;

        if (window.google) {

            directionsDisplay = new window.google.maps.DirectionsRenderer;

        } else {

            directionsDisplay = {
                setMap: () => {
                },
                setDirections: () => {
                }
            }

        }

        $scope.currentActiveTravelMode = _.first(travelModes);
        $scope.scheduleRightBeforeNextEvent = $scope.scheduleBeforeNextEvent || false;
        $scope.recalculateMakesSense = false;

        $scope.departureBufferMins = convertSecondsToMinutes($scope.departureBuffer || travelBufferOptionDefaults.departureBuffer);
        $scope.arrivalBufferMins = convertSecondsToMinutes($scope.arrivalBuffer || travelBufferOptionDefaults.arrivalBuffer);

        $scope.$watch('fromEvent', async (fromEvent) => {

            if (fromEvent) {

                recalculateTravelBuffer();

            }

        });

        $scope.showRouteForTravelMode = showRouteForTravelMode;

        $scope.recalculateTravelBuffer = async () => {
            await recalculateTravelBuffer();
        };

        $scope.selectRoute = () => {

            const travelBuffer = planningManager.createTravelBuffer($scope.travelBufferInfo, $scope.currentActiveTravelMode, $scope.scheduleRightBeforeNextEvent);

            $scope.onRouteSelected({
                travelBuffer
            });

        };

        $scope.scheduleBeforeNextEventCheckboxUpdated = async (newValue) => {

            $scope.scheduleRightBeforeNextEvent = newValue;
            recalculateTravelBuffer();

        };

        async function recalculateTravelBuffer() {

            $scope.canBeScheduleOnlyRightBeforeNextEvent = _.get($scope.fromEvent, 'isHomeEvent', false);
            $scope.scheduleRightBeforeNextEvent = $scope.canBeScheduleOnlyRightBeforeNextEvent || $scope.scheduleRightBeforeNextEvent;

            const travelBufferOptions = {
                departureBuffer: convertMinutesToSeconds($scope.departureBufferMins),
                arrivalBuffer: convertMinutesToSeconds($scope.arrivalBufferMins),
                scheduleBeforeNextEvent: $scope.scheduleRightBeforeNextEvent
            };

            $scope.travelBufferInfo = await planningManager.getTravelBufferFromTo($scope.fromEvent, $scope.toEvent, travelBufferOptions);
            $scope.routeSelectionTitle = $scope.travelBufferInfo.title;
            const groupedRoutes = _.get($scope.travelBufferInfo, 'groupedRoutes');

            $scope.showLoading = false;
            $scope.travelModesInfo = _.filter(_.map(travelModes, (travelMode) => getTravelModeInfo(groupedRoutes[travelMode])));
            $scope.selectedRouteDurationText = getTotalTravelDurationTextForTravelMode($scope.travelModesInfo, $scope.currentActiveTravelMode);

            showTravelInfoRoute(getTravelInfoForTravelMode($scope.currentActiveTravelMode));

            $scope.$digest();

        }

        function getTravelModeInfo(travelRouteData) {

            // structure of travelRouteData
            // - travelMode: travelMode string,
            // - duration: moment.duration,
            // - route: response from google directions API

            if (!travelRouteData) {
                return null;
            }

            return {
                ...travelRouteData,
                icon: travelModesIcons[travelRouteData.travelMode],
                durationText: getDurationText(travelRouteData.duration)
            };

        }

        function showRouteForTravelMode(travelMode) {

            $scope.currentActiveTravelMode = travelMode;
            $scope.selectedRouteDurationText = getTotalTravelDurationTextForTravelMode($scope.travelModesInfo, travelMode);
            showTravelInfoRoute(getTravelInfoForTravelMode(travelMode));

        }

        function showTravelInfoRoute(travelInfo) {

            if (travelInfo) {

                directionsDisplay.setDirections(travelInfo.route);

            } else {

                throw new Error('routeSelectionCtrl.showTravelInfoRoute() route is not defined to be shown on the map');

            }

        }

        function getTravelInfoForTravelMode(travelMode) {

            return _.find($scope.travelModesInfo, {travelMode: travelMode});

        }

        function getTotalTravelDurationTextForTravelMode(travelModesInfo, travelMode) {

            const travelDuration = _.get(_.find(travelModesInfo, {travelMode}), 'duration', '');
            const departureBufferSecs = convertMinutesToSeconds($scope.departureBufferMins);
            const arrivalBufferSecs = convertMinutesToSeconds($scope.arrivalBufferMins);
            const travelDurationWithBuffers = getTotalTravelDuration(travelDuration, departureBufferSecs, arrivalBufferSecs);
            return getDurationText(travelDurationWithBuffers);

        }

        function getTotalTravelDuration(duration, departureBuffer, arrivalBuffer) {

            return moment.duration(duration, 'seconds').add(departureBuffer || 0, 'seconds').add(arrivalBuffer || 0, 'seconds');

        }

        function getDurationText(momentDuration) {

            return momentDuration.format('h [hours] m [mins]');

        }

        function convertSecondsToMinutes(secs) {
            return Math.round(secs / 60);
        }

        function convertMinutesToSeconds(mins) {
            return mins * 60;
        }

        NgMap.getMap('route-selection-map').then(function (map) {
            directionsDisplay.setMap(map);
        });

    }

})();
