(function () {
    'use strict';

    angular.module('organizer.common.directives.routeSelection', ['organizer.common.services']);

    require('./controller');
    require('./directive');

})();