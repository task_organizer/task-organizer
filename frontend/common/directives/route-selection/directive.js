(function () {
    'use strict';

    angular.module('organizer.common.directives.routeSelection').directive('routeSelection', function () {
        return {
            restrict:    'E',
            scope:       {

                minutesDepartureBuffer:   '@',
                minutesArrivalBuffer:    '@',
                scheduleBeforeNextEvent: '@',

                fromEvent: '=',
                toEvent:   '=',

                onRouteSelected:          '&',
                onRouteSelectionCanceled: '&'

            },
            controller:  'routeSelectionCtrl',
            templateUrl: '/partials/common/directives/route-selection/view'
        };
    });

})();