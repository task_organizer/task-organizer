(function () {
    'use strict';

    angular.module('organizer.common.directives.dateTimePicker', []);

    require('./directive');

})();