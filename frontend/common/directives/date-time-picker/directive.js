(function () {
    'use strict';

    angular.module('organizer.common.directives.dateTimePicker').directive('dateTimePicker', function () {
        return {
            restrict: 'E',
            templateUrl: '/partials/common/directives/date-time-picker/view',
            scope: {
                ngModel: '=',
                minDate: '<',
                onDateSet: '&',
                isDisabled: '='
            },
            controller: [
                '$scope', function ($scope) {

                    $scope.dayPlanDatePickerid = 'dateTimePicker' + (new Date()).valueOf();
                    $scope.formattedDateTime = '';
                    $scope.datetimePickerConfig = {
                        dropdownSelector: `#${$scope.dayPlanDatePickerid}`,
                        minuteStep: 5,
                        minView: 'minute'
                    };

                    $scope.newDateSet = (newDate, oldDate) => {

                        $scope.onDateSet({date: newDate, oldDate});

                    };

                    $scope.$watch('ngModel', (newValue) => {

                        if (newValue) {
                            $scope.formattedDateTime = getFormattedDateTime($scope.ngModel);
                        }

                    });

                    function getFormattedDateTime(date) {

                        return moment(date).format('D MMMM, H:mm');

                    }

                }
            ]
        };
    });

})();