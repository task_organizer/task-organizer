(function () {
    'use strict';

    angular.module('organizer.common.directives.locationPicker')
        .controller('locationPickerCtrl', ['$scope', 'NgMap', 'geocoding', 'placesService', locationPickerController]);

    function locationPickerController($scope, NgMap, geocoding, placesService) {

        var mapControl = null;

        $scope.map = {center: {latitude: null, longitude: null}, zoom: 12};
        $scope.mapOptions = {scrollwheel: false};
        $scope.infoWindow = null;
        $scope.copyLocationToEdit = {};


        $scope.getLocationCoordinates = function (location) {

            return {
                latitude: location.latitude,
                longitude: location.longitude
            };

        };

        $scope.locationSelected = function (e, location) {

            $scope.selectedLocation = location;
            $scope.$digest();

        };

        $scope.deselectLocation = function () {

            $scope.$emit('removeSelectionFromLocation');

        };

        $scope.locationClicked = function (e, location) {

            showInfoWindow(location.name, location, location[$scope.infoWindowHeader], location[$scope.infoWindowContent]);

        };

        $scope.markerToEditDragEnd = function (locationChanges) {

            var latitude = locationChanges.latLng.lat();
            var longitude = locationChanges.latLng.lng();

            $scope.locationToEdit.latitude = latitude;
            $scope.locationToEdit.longitude = longitude;

            var callback = function (address) {

                $scope.locationToEdit.address = address;
                $scope.$apply();

            };

            geocoding.reverseGeocode(latitude, longitude, callback);

        };

        $scope.saveChangesToLocation = function () {

            $scope.saveEditFunction({location: $scope.locationToEdit});

        };

        $scope.cancelEditLocation = function () {

            angular.copy($scope.copyLocationToEdit, $scope.locationToEdit);
            $scope.$emit('stopEditingLocation', $scope.copyLocationToEdit);

        };

        $scope.searchForAddress = async function () {

            const {latitude, longitude} = await placesService.getAddressCoordinates($scope.locationToEdit.address);

            $scope.locationToEdit.latitude = latitude;
            $scope.locationToEdit.longitude = longitude;
            $scope.$digest();

            const center = new google.maps.LatLng($scope.locationToEdit.latitude, $scope.locationToEdit.longitude);

            mapControl.setCenter(center);
            mapControl.setZoom(15);

        };

        function showInfoWindow(event, position, header, content) {
            if ($scope.showInfoWindowOnClick) {

                if ($scope.infoWindow) {
                    $scope.infoWindow.close();
                }

                var infowindow = new google.maps.InfoWindow();
                $scope.infoWindow = infowindow;
                var center = new google.maps.LatLng(position.latitude, position.longitude);

                google.maps.event.addListener(infowindow, 'closeclick', function () {
                    $scope.$emit('removeSelectionFromLocation');
                });

                infowindow.setContent(
                    '<h4>' + header + '</h4>' +
                    '<p>' + content + '</p>');

                infowindow.setPosition(center);
                infowindow.open(mapControl);

            }
        }

        $scope.$watchCollection('locations', updateMap);
        $scope.$on('mapInitialized', updateMap);

        function updateMap() {
            if ($scope.locations && $scope.locations.length == 0) {
                getCurrentLocationBounds(function (bounds) {
                    centerMap(bounds, 12);
                });
            } else if ($scope.locations && $scope.locations.length == 1) {
                centerMap(getMarkersBounds(), 12);
            } else {
                centerMap(getMarkersBounds());
            }
        }

        function getCurrentLocationBounds(callback) {
            navigator.geolocation.getCurrentPosition(function (pos) {
                    var bounds = new google.maps.LatLngBounds();
                    var latlng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                    bounds.extend(latlng);
                    callback(bounds);
                },
                function (error) {
                    console.log('Unable to get location: ' + error.message);
                }, {enableHighAccuracy: false}
            );
        }

        // calculate bounds to show all locations on the map
        function getMarkersBounds() {

            const bounds = new google.maps.LatLngBounds();

            _.forEach($scope.locations, function (location) {
                const latlng = new google.maps.LatLng(location.latitude, location.longitude);
                bounds.extend(latlng);
            });

            return bounds;
        }

        $scope.$watch('isInEditMode', function (newValue) {

            if (newValue !== undefined) {
                angular.copy($scope.locationToEdit, $scope.copyLocationToEdit);
            }

        });

        async function centerMap(bounds, zoom) {
            if (bounds && angular.isFunction(bounds.getCenter) && !$scope.isInEditMode) {

                const map = await NgMap.getMap($scope.mapId);
                map.setCenter(bounds.getCenter());

                if (angular.isDefined(zoom)) {
                    map.setZoom(zoom);
                } else {
                    map.fitBounds(bounds);
                }


            }
        }

        NgMap.getMap($scope.mapId).then(function (map) {
            mapControl = map;
        });

    }

})();
