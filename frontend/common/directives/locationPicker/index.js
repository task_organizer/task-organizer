(function() {
    'use strict';

    require('ngmap');
    require('ng-map-autocomplete');
    require('./../../services');

    angular.module('organizer.common.directives.locationPicker', ['ngMap', 'organizer.common.services', 'ngMapAutocomplete', 'ui.bootstrap.datetimepicker']);

    require('./directive');
    require('./controller');

})();