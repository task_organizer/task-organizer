(function () {
    'use strict';

    angular.module('organizer.common.directives.locationPicker').directive('locationPicker', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                mapId: '@',
                locations: '=',
                selectedLocation: '=',
                locationToEdit: '=',
                isInEditMode: '=',
                highlightCurrentLocation: '@',
                locationToHighlight: '=',
                showInfoWindowOnClick: '@',
                infoWindowHeader: '@',
                infoWindowContent: '@',
                saveEditFunction: '&'
            },
            templateUrl: '/partials/common/directives/locationPicker/view',
            controller: 'locationPickerCtrl'
        };
    });

})();