(function () {
    'use strict';

    angular.module('organizer.common.directives.navbar', ['organizer.common.services', 'organizer.common.models']);

    require('./directive');

})();