(function () {
    'use strict';

    angular.module('organizer.common.directives.navbar').directive('navbar', function () {
        return {
            restrict:    'E',
            templateUrl: '/partials/common/directives/navbar/view',
            controller:  [
                '$scope', '$state', 'tasksManager', 'task', 'authManager', function ($scope, $state, tasksManager, Task, authManager) {

                    $scope.isNewTaskDialogVisible = false;
                    $scope.userName = authManager.getUserName();

                    $scope.toggleCreateQuickTaskPopup = function () {

                        $scope.newQuickTask = new QuickTask.create();

                    };

                    $scope.hideCreateQuickTaskPopup = function () {};

                    $scope.saveNewQuickTask = function () {

                        tasksManager.createQuickTask($scope.newQuickTask).then(function () {
                            alert('task created');
                        }, function () {
                            alert('error occurred');
                        }).then(function () {
                            $scope.hideCreateQuickTaskPopup();
                        });

                    };

                    $scope.showNewTaskModal = function() {

                        $scope.newTask = Task.create();
                        $scope.isNewTaskDialogVisible = true;

                    };

                    $scope.hideNewTaskModal = function() {

                        $scope.isNewTaskDialogVisible = false;

                    };

                    $scope.logout = function() {

                        authManager.logout();
                        $state.go('login');

                    };

                    $scope.resetNewTaskCreation = () => {
                        $scope.newTask = Task.create();
                    };

                    $scope.$on('newTaskDialog.taskCreated', () => {
                        $scope.hideNewTaskModal();
                        $scope.resetNewTaskCreation();
                    });

                }
            ]
        };
    });

})();