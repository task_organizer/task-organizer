(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name tumbusinessgame.common.directives.modal.directive:modalBody
     * @restrict E
     * @description This directive is responsible for displaying the modal body. This is achieved by specifying the insertion point of the transcluded DOM of the nearest parent directive that uses transclusion (ng-transclude).
     */
    angular.module('organizer.common.directives.modal')
        .directive('modalBody', function () {
            return {
                template:   '<div class="modal-body" ng-transclude></div>',
                replace:    true,
                restrict:   'E',
                transclude: true,
                scope: false
            };
        });

})();