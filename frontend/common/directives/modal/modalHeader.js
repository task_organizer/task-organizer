(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name tumbusinessgame.common.directives.modal.directive:modalHeader
     * @restrict E
     * @scope
     * @param {string} title Title of the modal
     * @description This directive is responsible for displaying the modal header.
     */
    angular.module('organizer.common.directives.modal')
        .directive('modalHeader', function () {
            return {
                templateUrl: '/partials/common/directives/modal/headerView',
                replace:     true,
                restrict:    'E',
                scope:       {title: '@'}
            };
        });

})();