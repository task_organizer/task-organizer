(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name tumbusinessgame.common.directives.modal.directive:modalFooter
     * @restrict E
     * @scope
     * @param {string} title Title of the modal
     * @description This directive is responsible for displaying the modal footer. This is achieved by specifying the insertion point of the transcluded DOM of the nearest parent directive that uses transclusion (ng-transclude).
     */
    angular.module('organizer.common.directives.modal')
        .directive('modalFooter', function () {
            return {
                template:   '<div class="modal-footer" ng-transclude></div>',
                replace:    true,
                restrict:   'E',
                transclude: true
            };
        });

})();