(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name tumbusinessgame.common.directives.modal.directive:modal
     * @restrict E
     * @scope
     * @param {boolean} visible Boolean value that will indicate whether the modal should be shown or not
     * @param {function} onShown A function containing actions to be performed when showing a modal
     * @param {function} onHide A function containing actions to be performed when hiding a modal
     * @param {Object} classNames CSS classes to be applied to the modal
     * @description This directive is responsible for showing and hiding modals.
     */
    angular.module('organizer.common.directives.modal')
        .directive('modal', function () {
                return {
                    templateUrl: '/partials/common/directives/modal/view',
                    restrict:    'E',
                    transclude:  true,
                    replace:     true,
                    scope:       {
                        visible:    '=',
                        onShown:    '&',
                        onHide:     '&',
                        classNames: '@'
                    },
                    link:        function postLink(scope, element, attrs) {

                        $(element).modal({
                            show:     scope.visible,
                            backdrop: attrs.backdrop
                        });

                        scope.$watch('visible', function (value) {

                            if (value) {
                                $(element).modal('show');
                            } else {
                                $(element).modal('hide');
                            }

                        });

                        $(element).on('shown.bs.modal', function () {

                            scope.onShown();
                            scope.$apply(function() {
                                scope.visible = true;
                            });

                        });

                        $(element).on('hidden.bs.modal', function () {

                            scope.onHide();
                            scope.$apply(function() {
                                scope.visible = false;
                            });

                        });
                    }
                };
            }
        );

})();