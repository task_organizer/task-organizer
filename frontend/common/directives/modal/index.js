(function() {

    'use strict';

    angular.module('organizer.common.directives.modal', []);

    require('./directive');
    require('./modalHeader');
    require('./modalBody');
    require('./modalFooter');


})();

