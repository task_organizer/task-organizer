(function () {
    'use strict';

    angular.module('organizer.common.directives.datePicker', []);

    require('./directive');

})();