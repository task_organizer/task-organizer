(function () {
    'use strict';

    angular.module('organizer.common.directives.datePicker').directive('datePicker', function () {
        return {
            restrict:    'E',
            templateUrl: '/partials/common/directives/date-picker/view',
            scope:       {
                ngModel:   '=',
                onDateSet: '&'
            },
            controller:  [
                '$scope', function ($scope) {

                    $scope.elementId = 'datePicker' + (new Date()).valueOf();
                    $scope.formattedDate = '';
                    $scope.datePickerConfig = { dropdownSelector: `#${$scope.elementId}`, minView :'day' };

                    $scope.newDateSet = (newDate, oldDate) => {

                        $scope.onDateSet({date: newDate, oldDate});

                    };

                    $scope.$watch('ngModel', (newValue) => {

                        if (newValue) {
                            $scope.formattedDate = getFormattedDate($scope.ngModel);
                        }

                    });

                    function getFormattedDate(date) {

                        return moment(date).format('dddd, D MMMM, Y')

                    }

                }
            ]
        };
    });

})();