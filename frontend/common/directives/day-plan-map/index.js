(function () {
    'use strict';

    angular.module('organizer.common.directives.dayPlanMap', ['organizer.common.services']);

    require('./controller');
    require('./directive');

})();