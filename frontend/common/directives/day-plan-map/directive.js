(function () {
    'use strict';

    angular.module('organizer.common.directives.dayPlanMap').directive('dayPlanMap', function () {
        return {
            restrict:    'E',
            scope:       {

                dayPlan: '='

            },
            controller:  'dayPlanMapCtrl',
            templateUrl: '/partials/common/directives/day-plan-map/view'
        };
    });

})();