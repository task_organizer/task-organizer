(function () {
    'use strict';

    var geodist = require('geodist');

    angular.module('organizer.common.directives.dayPlanMap')
        .controller('dayPlanMapCtrl', [
            '$scope',
            '$filter',
            'NgMap',
            routeSelectionCtrl
        ]);

    function routeSelectionCtrl($scope, $filter, NgMap) {

        const DirectionsDisplay = google.maps.DirectionsRenderer;
        let state = {
            directionsDisplays: [],
            mapBounds: new google.maps.LatLngBounds(),
            sortedEvents: []
        };

        $scope.mapId = 'day-plan-map' + (new Date().valueOf());

        $scope.$watchCollection('dayPlan.travelBuffers', async (travelBuffers) => {

            hideTravelBuffers();
            await showTravelBuffers(travelBuffers);

        });

        $scope.$watch('dayPlan', recalculateBounds);
        $scope.$watchCollection('dayPlan.events', dayPlanEventsUpdated);
        $scope.$watchCollection('dayPlan.todos', recalculateBounds);

        $scope.$on('$destroy', function () {
            hideTravelBuffers();
        });

        $scope.getEventOrderNumber = (event) => {
            return _.indexOf(state.sortedEvents, event) + 1;
        };

        $scope.getFormattedEventStartTime = (event) => {
            return $filter('momentTimeFormatter')(event.start);
        };

        async function getMap() {
            return await NgMap.getMap($scope.mapId);
        }

        function recalculateBounds(objects) {

            if (objects && objects.length) {

                const objectsWithLocation = _.filter(objects, (obj) => !(_.isNaN(obj.latitude) && _.isNaN(obj.longitude)));

                let bounds = getBoundsFor(objectsWithLocation);
                state.mapBounds = state.mapBounds.union(bounds);

                fitBounds();

            }

        }

        function dayPlanEventsUpdated(events) {
            updateSortedEvents(events);
            recalculateBounds(events);
        }

        function updateSortedEvents(events) {
            state.sortedEvents = _.sortBy(events, ['start']);
        }

        async function fitBounds() {

            const map = await getMap();
            map.setCenter(state.mapBounds.getCenter());
            map.fitBounds(state.mapBounds);

            const options = {
                unit: 'meters'
            };

            const origin = {
                lat: state.mapBounds.getNorthEast().lat(),
                lon: state.mapBounds.getNorthEast().lng()
            };

            const destination = {
                lat: state.mapBounds.getSouthWest().lat(),
                lon: state.mapBounds.getSouthWest().lng()
            };

            if (geodist(origin, destination, options) < 500) {
                map.setZoom(13);
            }

        }

        function getBoundsFor(objectsWithLocation) {

            let initialBounds = new google.maps.LatLngBounds();

            return _.reduce(objectsWithLocation, (calculatedBounds, objectWithLocation) => {

                const location = _.get(objectWithLocation, 'location');
                if (location && _.isNumber(location.latitude) && _.isNumber(location.longitude)) {

                    return calculatedBounds.extend({
                        lat: location.latitude,
                        lng: location.longitude
                    });

                } else {

                    return calculatedBounds;

                }

            }, initialBounds);

        }

        function hideTravelBuffers() {

            _.forEach(state.directionsDisplays, (directionsDisplay) => {
                directionsDisplay.setMap(null);
            });

        }

        async function showTravelBuffers(travelBuffers) {

            const map = await getMap();
            state.directionsDisplays = _.map(travelBuffers, constructDirectionsDisplayForTravelBuffer);

            _.each(state.directionsDisplays, (directionsDisplay) => {
                directionsDisplay.setMap(map);
            });

        }

        function constructDirectionsDisplayForTravelBuffer(travelBuffer) {

            const travelMode = _.get(travelBuffer, 'travelMode');
            const directionRoute = _.get(travelBuffer, `directions.${travelMode}.route`);

            if (directionRoute) {

                const directionsDisplay = new DirectionsDisplay();
                directionsDisplay.setOptions({
                    suppressMarkers: true,
                    preserveViewport: true,
                    suppressBicyclingLayer: true
                });
                directionsDisplay.setDirections(directionRoute);

                return directionsDisplay;

            } else {

                throw new Error('dayPlanMapCtrl.constructDirectionsDisplayForTravelBuffer() - no directions to be shown on the map');

            }

        }

    }

})();
