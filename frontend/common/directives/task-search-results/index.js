(function () {
    'use strict';

    angular.module('organizer.common.directives.taskSearchResults', []);

    require('./directive');

})();