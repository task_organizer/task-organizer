(function () {
    'use strict';

    angular.module('organizer.common.directives.taskSearchResults').directive('taskSearchResults', function () {
        return {
            restrict:    'E',
            templateUrl: '/partials/common/directives/task-search-results/view',
            scope:       {
                query: '=',
                noResultsMessage: '@'
            },
            controller:  [
                '$scope', 'tasksManager', ($scope, tasksManager) => {

                    $scope.$watch('query', async (newQuery) => {

                        if (_.keys(newQuery).length) {

                            $scope.tasks = await tasksManager.getTasks(newQuery);
                            $scope.$apply();

                        }

                    });

                }
            ]
        };
    });

})();