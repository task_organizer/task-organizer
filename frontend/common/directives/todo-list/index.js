(function () {
    'use strict';

    angular.module('organizer.common.directives.todoList', []);

    require('./directive');

})();