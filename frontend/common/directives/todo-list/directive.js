(function () {
    'use strict';

    angular.module('organizer.common.directives.todoList').directive('todoList', function () {
        return {
            restrict: 'E',
            templateUrl: '/partials/common/directives/todo-list/view',
            scope: {
                taskTitleProperty: '@',
                showCheckedState: '@',
                showRemoveButton: '@',
                supportDroppingEvent: '@',
                supportReorder: '@',

                todos: '=',
                checkedPredicate: '=',

                onDropCallback: '&',
                onRemoveButtonClicked: '&',
                updateOrderCallback: '&',
                updateStateForTaskCallback: '&'
            },
            controller: [
                '$scope', ($scope) => {

                    $scope.$watchCollection('todos', (updatedTodos) => {
                        if ($scope.showCheckedState) {
                            _.each(updatedTodos, (todo) => {
                                _.extend(todo, {isDone: todo.status === 'done'})
                            });
                        }
                    });

                    $scope.droppedCallback = (taskId) => {
                        $scope.onDropCallback({taskId});
                    };

                    $scope.updateTodoStatusIsDone = (task, newValue) => {

                        task.status = newValue ? 'done' : 'scheduled';
                        $scope.updateStateForTaskCallback({task: task});

                    }

                }
            ]
        };
    });

})();