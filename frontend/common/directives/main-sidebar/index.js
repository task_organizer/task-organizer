(function () {
    'use strict';

    angular.module('organizer.common.directives.mainSidebar', ['organizer.common.services']);

    require('./directive');

})();