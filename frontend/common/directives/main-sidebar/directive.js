(function () {
    'use strict';

    angular.module('organizer.common.directives.mainSidebar').directive('mainSidebar', function () {
        return {
            restrict:    'E',
            templateUrl: '/partials/common/directives/main-sidebar/view',
            scope:       true,
            replace:     true,
            controller:  [
                '$scope', 'categoriesManager', function ($scope, categoriesManager) {

                    $scope.numberOfVisibleCategories = 7;
                    $scope.dayAfterTomorrowDate = moment().add(2, 'days').format('D-MMM-Y');

                    categoriesManager.getAllCategories().then(function (categories) {

                        $scope.categories = categories;

                    });

                    $scope.showMoreCategories = () => {

                        $scope.numberOfVisibleCategories += 7;

                    };

                }
            ]
        };
    });

})();