(function () {
    'use strict';

    angular.module('organizer.common.directives.tooltip', []);

    require('./directive');

})();