(function () {
    'use strict';

    angular.module('organizer.common.directives.todoList').directive('tooltip', function () {
        return {
            restrict:    'E',
            transclude:  {
                summary: 'tooltipSummary',
                details: 'tooltipDetails'
            },
            scope:       {
                toggleDetailsTooltip: '=',
                hideDetailsTooltip:   '='
            },
            templateUrl: '/partials/common/directives/tooltip/view',
            controller:  [
                '$scope', function ($scope) {

                    const oldToggleTooltipFunc = $scope.toggleDetailsTooltip;

                    $scope.__isDetailsTooltipVisible = false;

                    $scope.toggleDetailsTooltip = function () {

                        $scope.__isDetailsTooltipVisible = !$scope.__isDetailsTooltipVisible;

                        if (oldToggleTooltipFunc) {

                            oldToggleTooltipFunc();

                        }
                    };

                    $scope.hideDetailsTooltip = function () {
                        $scope.__isDetailsTooltipVisible = false;
                    };

                }
            ]
        };
    });

})();