(function() {
    angular.module('organizer.common.formatters', []);

    require('./momentTimeFormatter');
    require('./travelBufferDurationFormatter');
})();