'use strict';

(function () {

    angular.module('organizer.common.formatters').filter('travelBufferDurationFormatter', function () {
        return function (title) {

            const duration = _.first(title.match(/\(.+?\)/));
            return duration.replace(/\(|\)/g, '');

        };
    })

})();