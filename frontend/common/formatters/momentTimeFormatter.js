'use strict';

(function () {

    angular.module('organizer.common.formatters').filter('momentTimeFormatter', function () {
        return function (date) {
            return moment(date).format('H:mm');
        };
    })

})();