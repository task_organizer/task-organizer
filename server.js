// modules =================================================
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');

// configuration ===========================================

// config files
var db = require('./config/db');

mongoose.Promise = global.Promise;
mongoose.connect(db.url, {useMongoClient: true});
mongoose.connection.on('error', function () {
    console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
    process.exit(1);
});

var port = process.env.PORT || 8888; // set our port

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json({limit: '50mb'})); // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({extended: true})); // parse application/x-www-form-urlencoded

app.set('views', path.join(__dirname, '/frontend'));
app.set('view engine', 'jade');

// routes ==================================================
require('./app/routes')(app); // pass our application into our routes

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT

// start app ===============================================
app.listen(port);
console.log('Magic happens on port ' + port); 			// shoutout to the user
exports = module.exports = app; 						// expose app